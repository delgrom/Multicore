#**************************************************************
# This .sdc file is created by Terasic Tool.
# Users are recommended to modify this file to match users logic.
#**************************************************************

#**************************************************************
# Create Clock
#**************************************************************
create_clock -period 20 [get_ports clock_50_i]

#**************************************************************
# Create Generated Clock
#**************************************************************
derive_pll_clocks



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************
derive_clock_uncertainty



#**************************************************************
# Set Input Delay
#**************************************************************

set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[0]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[1]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[2]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[3]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[4]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[5]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[6]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[7]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[8]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[9]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[10]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[11]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[12]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[13]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[14]}]
set_input_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[2]}]  3.000 [get_ports {SDRAM_DQ[15]}]


#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[0]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[1]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[2]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[3]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[4]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[5]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[6]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[7]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[8]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[9]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[10]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[11]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_A[12]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_BA[0]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_BA[1]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_nCAS}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQML}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQMH}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[0]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[1]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[2]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[3]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[4]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[5]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[6]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[7]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[8]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[9]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[10]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[11]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[12]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[13]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[14]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_DQ[15]}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_nRAS}]
set_output_delay -add_delay  -clock [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  3.000 [get_ports {SDRAM_nWE}]



#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************
set_false_path  -from  [get_clocks {sys_inst|dcm_cpu_inst|altpll_component|auto_generated|pll1|clk[0]}]  -to  [get_clocks {sys_inst|dcm_cpu_inst|altpll_component|auto_generated|pll1|clk[1]}]
set_false_path  -from  [get_clocks {sys_inst|dcm_cpu_inst|altpll_component|auto_generated|pll1|clk[1]}]  -to  [get_clocks {sys_inst|dcm_cpu_inst|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path  -from  [get_clocks {sys_inst|dcm_cpu_inst|altpll_component|auto_generated|pll1|clk[0]}]  -to  [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path  -from  [get_clocks {sys_inst|dcm_cpu_inst|altpll_component|auto_generated|pll1|clk[0]}]  -to  [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]
set_false_path  -from  [get_clocks {sys_inst|dcm_cpu_inst|altpll_component|auto_generated|pll1|clk[0]}]  -to  [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[3]}]
set_false_path  -from  [get_clocks {sys_inst|dcm_cpu_inst|altpll_component|auto_generated|pll1|clk[0]}]  -to  [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[4]}]
set_false_path  -from  [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[0]}] -to [get_clocks {sys_inst|dcm_cpu_inst|altpll_component|auto_generated|pll1|clk[0]}]  
set_false_path  -from  [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}] -to [get_clocks {sys_inst|dcm_cpu_inst|altpll_component|auto_generated|pll1|clk[0]}]  
set_false_path  -from  [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[3]}] -to [get_clocks {sys_inst|dcm_cpu_inst|altpll_component|auto_generated|pll1|clk[0]}]  
set_false_path  -from  [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[4]}] -to [get_clocks {sys_inst|dcm_cpu_inst|altpll_component|auto_generated|pll1|clk[0]}]  
set_false_path  -from  [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[0]}] -to [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}]  
set_false_path  -from  [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[1]}] -to [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[0]}]  
set_false_path -from [get_clocks {clock_50_i}] -to [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[3]}]
set_false_path -from [get_clocks {sys_inst|dcm_system|altpll_component|auto_generated|pll1|clk[3]}] -to [get_clocks {clock_50_i}]

set_false_path -from [get_registers {system:sys_inst|VGA_SC:sc|planarreq}]

#**************************************************************
# Set Multicycle Path
#**************************************************************
set_multicycle_path -from [get_registers {sys_inst|CPUUnit|cpu*}] -setup -start 2
set_multicycle_path -from [get_registers {sys_inst|CPUUnit|cpu*}] -hold -start 1


#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************



#**************************************************************
# Set Load
#**************************************************************



# How to change the PLL frequency after compilation
# 1 - Locate the PLL in the hierarchy, right click and select <Locate Node>/<Locate in Resource Property Editor>
# 2 - Change the desired parameters (the non grayed ones)
# 3 - In the main menu select <Edit>/<Check and Save all Netlist Changes>
# 4 - Wait for Fitter and Assembler