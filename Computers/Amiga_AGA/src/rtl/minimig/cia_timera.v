/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/module cia_timera
(
  input   clk,            // clock
  input clk7_en,
  input  wr,            // write enable
  input   reset,           // reset
  input   tlo,          // timer low byte select
  input  thi,           // timer high byte select
  input  tcr,          // timer control register
  input   [7:0] data_in,      // bus data in
  output   [7:0] data_out,      // bus data out
  input  eclk,            // count enable
  output  tmra_ovf,        // timer A underflow
  output  spmode,          // serial port mode
  output  irq            // intterupt out
);

reg    [15:0] tmr;        // timer
reg    [7:0] tmlh;        // timer latch high byte
reg    [7:0] tmll;        // timer latch low byte
reg    [6:0] tmcr;        // timer control register
reg    forceload;        // force load strobe
wire  oneshot;        // oneshot mode
wire  start;          // timer start (enable)
reg    thi_load;          // load tmr after writing thi in one-shot mode
wire  reload;          // reload timer counter
wire  zero;          // timer counter is zero
wire  underflow;        // timer is going to underflow
wire  count;          // count enable signal

// count enable signal
assign count = eclk;

// writing timer control register
always @(posedge clk)
  if (clk7_en) begin
    if (reset)  // synchronous reset
      tmcr[6:0] <= 7'd0;
    else if (tcr && wr)  // load control register, bit 4(strobe) is always 0
      tmcr[6:0] <= {data_in[6:5],1'b0,data_in[3:0]};
    else if (thi_load && oneshot)  // start timer if thi is written in one-shot mode
      tmcr[0] <= 1'd1;
    else if (underflow && oneshot) // stop timer in one-shot mode
      tmcr[0] <= 1'd0;
  end

always @(posedge clk)
  if (clk7_en) begin
    forceload <= tcr & wr & data_in[4];  // force load strobe
  end

assign oneshot = tmcr[3];    // oneshot alias
assign start = tmcr[0];      // start alias
assign spmode = tmcr[6];    // serial port mode (0-input, 1-output)

// timer A latches for high and low byte
always @(posedge clk)
  if (clk7_en) begin
    if (reset)
      tmll[7:0] <= 8'b1111_1111;
    else if (tlo && wr)
      tmll[7:0] <= data_in[7:0];
  end

always @(posedge clk)
  if (clk7_en) begin
    if (reset)
      tmlh[7:0] <= 8'b1111_1111;
    else if (thi && wr)
      tmlh[7:0] <= data_in[7:0];
  end

// thi is written in one-shot mode so tmr must be reloaded
always @(posedge clk)
  if (clk7_en) begin
    thi_load <= thi & wr & (~start | oneshot);
  end

// timer counter reload signal
assign reload = thi_load | forceload | underflow;

// timer counter
always @(posedge clk)
  if (clk7_en) begin
    if (reset)
      tmr[15:0] <= 16'hFF_FF;
    else if (reload)
      tmr[15:0] <= {tmlh[7:0],tmll[7:0]};
    else if (start && count)
      tmr[15:0] <= tmr[15:0] - 16'd1;
  end

// timer counter equals zero
assign zero = ~|tmr;

// timer counter is going to underflow
assign underflow = zero & start & count;

// Timer A underflow signal for Timer B
assign tmra_ovf = underflow;

// timer underflow interrupt request
assign irq = underflow;

// data output
assign data_out[7:0] = ({8{~wr&tlo}} & tmr[7:0])
          | ({8{~wr&thi}} & tmr[15:8])
          | ({8{~wr&tcr}} & {1'b0,tmcr[6:0]});


endmodule

