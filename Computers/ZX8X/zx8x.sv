/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
// 
//  ZX80-ZX81 replica for MiST
//  Copyright (C) 2018 György Szombathelyi
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//============================================================================

//============================================================================
//
//  Multicore 2 Top by Victor Trucco
//
//============================================================================

`default_nettype none


module zx8x
(
    // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAMs (AS7C34096)
    output wire [18:0]sram_addr_o  = 18'b0000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM    (H57V256)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io          = 1'bz,
    inout wire  ps2_data_io         = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o           = 1'b1,
    output wire sd_sclk_o           = 1'b0,
    output wire sd_mosi_o           = 1'b0,
    input wire  sd_miso_i,

    // Joysticks
    input wire  joy1_up_i,
    input wire  joy1_down_i,
    input wire  joy1_left_i,
    input wire  joy1_right_i,
    input wire  joy1_p6_i,
    input wire  joy1_p9_i,
    input wire  joy2_up_i,
    input wire  joy2_down_i,
    input wire  joy2_left_i,
    input wire  joy2_right_i,
    input wire  joy2_p6_i,
    input wire  joy2_p9_i,
    output wire joyX_p7_o           = 1'b1,

    // Audio
    output        AUDIO_L,
    output        AUDIO_R,
    input wire  ear_i,
    output wire mic_o                   = 1'b0,

        // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

        // HDMI
    output wire [7:0]tmds_o         = 8'b00000000,

        //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
        
    inout wire  stm_b8_io, 
    inout wire  stm_b9_io,

    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2
);

//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign stm_rst_o    = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

assign joyX_p7_o = 1'b1;

//assign LED = ~ioctl_download & ~tape_ready;

`include "build_id.v"
localparam CONF_STR = 
{
    "P,ZX81.dat;",
    "S1,O/P,Load *.P;",
    "O9,Scanlines,Off,On;",
    "O7,Inverse video,Off,On;",
    "O4,Model,ZX81,ZX80;",
    "OAB,RAM size,1k,16k,32k,64k;",
    "OC,CHR128 support,Off,On;",
//    "O8,Swap joy axle,Off,On;",
    "O6,Video frequency,50Hz,60Hz;",
    "TG,Reset;",
    "V,v1.0.",`BUILD_DATE
};

// ROM Data Pump
reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_sys, ~locked, ioctl_download, pump_s);

////////////////////   CLOCKS   ///////////////////
wire clk_sys;
wire locked;

pll pll
(
    .inclk0(clock_50_i),
    .c0(clk_sys), //52 MHz
    .c1(SDRAM_CLK),
    .locked(locked)
);

//assign SDRAM_CLK = clk_sys;


reg  ce_cpu_p;
reg  ce_cpu_n;
reg  ce_13,ce_65,ce_psg;

always @(negedge clk_sys) begin
    reg [4:0] counter = 0;

    counter  <=  counter + 1'd1;
    ce_cpu_p <= !counter[3] & !counter[2:0];
    ce_cpu_n <=  counter[3] & !counter[2:0];
    ce_65    <= !counter[2:0];
    ce_13    <= !counter[1:0];
    ce_psg   <= !counter[4:0];
end

//////////////////   MIST ARM I/O   ///////////////////
wire [10:0] ps2_key;
wire [24:0] ps2_mouse;

wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire  [1:0] buttons;
wire  [1:0] switches;
wire        scandoubler_disable;
wire        ypbpr;
wire [31:0] status;

wire        ioctl_wr;
wire [13:0] ioctl_addr;
wire  [7:0] ioctl_dout;
wire        ioctl_download;
wire  [7:0] ioctl_index;

/*
mist_io #(.STRLEN($size(CONF_STR)>>3)) user_io
(
    .clk_sys(clk_sys),
    .CONF_DATA0(CONF_DATA0),
    .SPI_SCK(SPI_SCK),
    .SPI_DI(SPI_DI),
    .SPI_DO(SPI_DO),
    .SPI_SS2(SPI_SS2),
    
    .conf_str(CONF_STR),

    .status(status),
    .scandoubler_disable(scandoubler_disable),
    .ypbpr(ypbpr),
    .buttons(buttons),
    .switches(switches),
    .joystick_0(joystick_0),
    .joystick_1(joystick_1),
    .ps2_key(ps2_key),

    .sd_conf(0),
    .sd_sdhc(1),
    .ioctl_ce(1),
    .ioctl_wr(ioctl_wr),
    .ioctl_addr(ioctl_addr),
    .ioctl_dout(ioctl_dout),
    .ioctl_download(ioctl_download),
    .ioctl_index(ioctl_index),

    // unused
    .ps2_kbd_clk(),
    .ps2_kbd_data(),
    .ps2_mouse_clk(),
    .ps2_mouse_data(),
    .joystick_analog_0(),
    .joystick_analog_1(),
    .sd_ack_conf()
);
*/

wire [7:0] osd_o;

data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( pump_s & osd_o ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),
    
    .ioctl_download( ioctl_download  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);

///////////////////   CPU   ///////////////////
wire [15:0] addr;
wire  [7:0] cpu_din;
wire  [7:0] cpu_dout;
wire        nM1;
wire        nMREQ;
wire        nIORQ;
wire        nRD;
wire        nWR;
wire        nRFSH;
wire        nHALT;
wire        nINT = addr[6];
wire        nNMI;
wire        nWAIT;
reg         reset;

T80pa cpu
(
    .RESET_n(~reset),
    .CLK(clk_sys),
    .CEN_p(ce_cpu_p),
    .CEN_n(ce_cpu_n),
    .WAIT_n(nWAIT),
    .INT_n(nINT),
    .NMI_n(nNMI),
    .BUSRQ_n(1),
    .M1_n(nM1),
    .MREQ_n(nMREQ),
    .IORQ_n(nIORQ),
    .RD_n(nRD),
    .WR_n(nWR),
    .RFSH_n(nRFSH),
    .HALT_n(nHALT),
    .A(addr),
    .DO(cpu_dout),
    .DI(cpu_din)
);

wire [7:0] io_dout = kbd_n ? (psg_sel ? psg_out : 8'hFF) : { tape_in, hz50, 1'b0, key_data[4:0] & ({5{addr[12]}} | ~joykeys_A12) & ({5{addr[11]}} | ~joykeys_A11)};

always_comb begin
    case({nMREQ, ~nM1 | nIORQ | nRD})
        'b01: cpu_din = (~nM1 & nopgen) ? 8'h0 : mem_out;
        'b10: cpu_din = io_dout;
     default: cpu_din = 8'hFF;
    endcase
end

wire       tape_in = ~ear_i;
reg        init_reset = 1;
reg        zx81 = 1'b1;
reg  [1:0] mem_size = 2'b11; //00-1k, 01 - 16k 10 - 32k
initial 
begin
    mem_size = 2'b11; 
    zx81 = 1'b1;
end

wire       hz50 = ~status[6];
wire [4:0] joykeys_A12 = {~joy1_down_i, ~joy1_up_i, ~joy1_right_i, 1'b0, ~(joy1_p6_i & joy1_p9_i)};
wire [4:0] joykeys_A11 = {~joy1_left_i, 4'b0000};

reg [26:0] PoR_cnt;
initial 
begin
    PoR_cnt = 27'b110001100101110101000000000; // 2 sec
end

reg PoR_reset;
always @(posedge clk_sys) 
begin
    if(PoR_cnt > 27'd0)
    begin
        PoR_cnt = PoR_cnt - 27'd1;
        PoR_reset <= 1'b1;
    end
    else
        PoR_reset <= 1'b0;
end 

always @(posedge clk_sys) begin
    reg old_download;
    old_download <= ioctl_download;
    if(~ioctl_download && old_download && !ioctl_index) init_reset <= 0;
    if(~ioctl_download && old_download && ioctl_index) tape_ready <= 1;
    
    reset <= ~btn_n_i[4] | status[16] | (mod[1] & Fn[11]) | init_reset | PoR_reset;
    if (reset) begin
        zx81 <= ~status[4];
        mem_size <= status[11:10];
        tape_ready <= 0;
    end
end

//////////////////   MEMORY   //////////////////

sdram ram
(
    .*,
    .init(~locked),
    .clk(clk_sys),
    .wtbt(0),
    .dout(ram_out),
    .din (ram_in),
    .addr(ram_a),
    .we(ram_we | tapewrite_we),
    .rd(ram_e & (~nRFSH | (~nRD & ~nMREQ)) & ~ce_cpu_n & ~tapeloader),
    .ready(ram_ready)
);

wire        ram_ready;
reg   [7:0] rom[12288];
wire [12:0] rom_a  = nRFSH ? addr[12:0] : { addr[12:9], ram_data_latch[5:0], row_counter };
//wire [15:0] tape_load_addr = 16'h4000 + ((ioctl_index[7:6] == 1) ? tape_addr + 4'd8 : tape_addr-1'd1);
wire [15:0] tape_load_addr = 16'h4000 + ((zx81 == 1) ? tape_addr + 4'd8 : tape_addr-1'd1); // $4000(.o file ) $4009 (.p file)
wire [15:0] ram_a;
wire        ram_e_64k = &mem_size & (addr[13] | (addr[15] & nM1));
wire        rom_e  = ~addr[14] & (~addr[12] | zx81) & ~ram_e_64k & ~chr128_mem;
wire        ram_e  = addr[14] | ram_e_64k | chr128_mem;
wire        ram_we = ~nWR & ~nMREQ & ram_e;
wire  [7:0] ram_in = tapeloader ? tape_in_byte : cpu_dout;
wire  [7:0] rom_out;
wire  [7:0] ram_out;
wire  [7:0] mem_out;

wire        chr128 = status[12];
wire        chr128_mem = chr128 & (addr[15:12] == 4'h3);

always_comb begin
    casex({ tapeloader, rom_e, ram_e })
        'b110: mem_out = tape_loader_patch[addr - (zx81 ? 13'h0347 : 13'h0207)];
        'b010: mem_out = rom_out;
        'b001: mem_out = ram_out;
        default: mem_out = 8'd0;
    endcase

    casex({tapeloader, mem_size, chr128_mem })
        'b1_XX_X: ram_a = tape_load_addr;
        'b0_00_0: ram_a = { 6'b010000,             addr[9:0] }; //1k
        'b0_01_0: ram_a = { 2'b01,                addr[13:0] }; //16k
        'b0_10_0: ram_a = { 1'b0, addr[15] & nM1, addr[13:0] } + 16'h4000; //32k
        'b0_11_0: ram_a = { addr[15] & nM1,       addr[14:0] }; //64k
        'b0_XX_1: ram_a = nRFSH ? addr[15:0] : { addr[15:10], addr[8] & ram_data_latch[7], ram_data_latch[5:0], row_counter }; //chr
    endcase
end

always @(posedge clk_sys) begin
    if (ioctl_wr & !ioctl_index) begin
        rom[ioctl_addr] <= ioctl_dout;
    end
end

always @(posedge clk_sys) begin
    rom_out <= rom[{ (zx81 ? rom_a[12] : 2'h2), rom_a[11:0] }];
end

////////////////////  TAPE  //////////////////////
reg   [7:0] tape_ram[16384];
reg         tapeloader, tapewrite_we;
reg  [13:0] tape_addr;
reg   [7:0] tape_in_byte;
reg         tape_ready;  // there is data in the tape memory
// patch the load ROM routines to loop until the memory is filled from $4000(.o file ) $4009 (.p file)
// xor a; loop: nop or scf, jr nc loop, jp h0207 (jp h0203 - ZX80)
reg   [7:0] tape_loader_patch[7] = '{8'haf, 8'h00, 8'h30, 8'hfd, 8'hc3, 8'h07, 8'h02};

always @(posedge clk_sys) begin
    if (ioctl_wr & ioctl_index) begin
        tape_ram[ioctl_addr] <= ioctl_dout;
    end
end

always @(posedge clk_sys) begin
    tape_in_byte <= tape_ram[tape_addr];
end

always @(posedge clk_sys) begin
    reg old_nM1;
    
    old_nM1 <= nM1;
    tapewrite_we <= 0;
    
    if (~nM1 & old_nM1 & tape_ready) begin
        if (zx81) begin
            if (addr == 16'h0347) begin
                tape_loader_patch[1] <= 8'h00; //nop
                tape_loader_patch[5] <= 8'h07; //0207h
                tape_addr <= 14'h0;
                tapeloader <= 1;
            end
            if (addr >= 16'h03c3 || addr < 16'h0347) begin
                tapeloader <= 0;
            end
        end else begin
            if (addr == 16'h0207) begin
                tape_loader_patch[1] <= 8'h00; //nop
                tape_loader_patch[5] <= 8'h03; //0203h
                tape_addr <= 14'h0;
                tapeloader <= 1;
            end
            if (addr >= 16'h024d || addr < 16'h0207) begin
                tapeloader <= 0;
            end
        end
    end

    if (tapeloader & ce_cpu_p) begin
        if (tape_addr != ioctl_addr) begin
            tape_addr <= tape_addr + 1'h1;
            tapewrite_we <= 1;
        end else begin
            tape_loader_patch[1] <= 8'h37; //scf
        end
    end
end

////////////////////  VIDEO //////////////////////
// Based on the schematic:
// http://searle.hostei.com/grant/zx80/zx80.html

// character generation
wire      nopgen = addr[15] & ~mem_out[6] & nHALT;
wire      data_latch_enable = nRFSH & ce_cpu_n & ~nMREQ;
reg [7:0] ram_data_latch;
reg       nopgen_store;
reg [2:0] row_counter;
wire      shifter_start = nMREQ & nopgen_store & ce_cpu_p & (~zx81 | ~NMIlatch);
reg [7:0] shifter_reg;
wire      video_out = (~status[7] ^ shifter_reg[7] ^ inverse) & !back_porch_counter & csync;
reg       inverse;

reg[4:0]  back_porch_counter = 1;

always @(posedge clk_sys) begin
    reg old_csync;
    reg old_shifter_start;
    
    old_csync <= csync;
    old_shifter_start <= shifter_start;

    if (data_latch_enable) begin
        ram_data_latch <= mem_out;
        nopgen_store <= nopgen;
    end

    if (nMREQ & ce_cpu_p) inverse <= 0;

    if (~old_shifter_start & shifter_start) begin
        shifter_reg <= (~nM1 & nopgen) ? 8'h0 : mem_out;
        inverse <= ram_data_latch[7];
    end else if (ce_65) begin
        shifter_reg <= { shifter_reg[6:0], 1'b0 };
    end

    if (old_csync & ~csync) row_counter <= row_counter + 1'd1;
    if (~vsync) row_counter <= 0;

    if (~old_csync & csync) back_porch_counter <= 1;
   if (ce_65 && back_porch_counter) back_porch_counter <= back_porch_counter + 1'd1;

    end

// ZX80 sync generator
reg ic11,ic18,ic19_1,ic19_2;
//wire csync = ic19_2; //ZX80 original
wire csync = vsync & hsync;
wire vsync = ic11;

always @(posedge clk_sys) begin

    reg old_nM1;
    old_nM1 <= nM1;

    if (~(nIORQ | nWR) & (~zx81 | ~NMIlatch)) ic11 <= 1; // stop vsync - any OUT
    if (~kbd_n & (~zx81 | ~NMIlatch)) ic11 <= 0; // start vsync - keyboard IN

    if (~nIORQ) ic18 <= 1;  // if IORQ - preset HSYNC start
    if (~ic19_2) ic18 <= 0; // if sync active - preset sync end

    // And 2 M1 later the presetted sync arrives at the csync pin
    if (old_nM1 & ~nM1) begin
        ic19_1 <= ~ic18;
        ic19_2 <= ic19_1;
    end
    if (~ic11) ic19_2 <= 0; //vsync keeps csync low
end

// ZX81 upgrade
// http://searle.hostei.com/grant/zx80/zx80nmi.html

wire      hsync = ~(sync_counter >= 16 && sync_counter <= 31);
reg       NMIlatch;
reg [7:0] sync_counter = 0;

assign nWAIT = ~(nHALT & ~nNMI) | ~zx81;
assign nNMI = ~(NMIlatch & ~hsync) | ~zx81;

always @(posedge clk_sys) begin
    reg       old_cpu_n;

    old_cpu_n <= ce_cpu_n;

    if (old_cpu_n & ~ce_cpu_n) begin
        sync_counter <= sync_counter + 1'd1;
       if (sync_counter == 8'd206 | (~nM1 & ~nIORQ)) sync_counter <= 0;
    end

    if (zx81) begin
        if (~nIORQ & ~nWR & (addr[0] ^ addr[1])) NMIlatch <= addr[1];
    end
end

wire       v_sd_out, HS_sd_out, VS_sd_out;

scandoubler scandoubler
(
    .clk(clk_sys),
    .ce_2pix(ce_13),

    .scanlines(status[9]),

    .csync(csync),
    .v_in(video_out),

    .hs_out(HS_sd_out),
    .vs_out(VS_sd_out),
    .v_out(v_sd_out)
);

wire [5:0] R_out,G_out,B_out;
osd1 osd1
(
    .*,
    .SPI_SS3 (SPI_SS2),
    .R_in((scandoubler_disable ? video_out : v_sd_out) ? 6'b111111 : 6'd0),
    .G_in((scandoubler_disable ? video_out : v_sd_out) ? 6'b111111 : 6'd0),
    .B_in((scandoubler_disable ? video_out : v_sd_out) ? 6'b111111 : 6'd0),
    
    .R_out(R_out),
    .G_out(G_out),
    .B_out(B_out),
    .HSync(scandoubler_disable ? hsync : HS_sd_out),
    .VSync(scandoubler_disable ? vsync : VS_sd_out)
);

wire [5:0] y, pb, pr;
rgb2ypbpr rgb2ypbpr 
(
    .red   ( R_out ),
    .green ( G_out ),
    .blue  ( B_out ),
    .y     ( y     ),
    .pb    ( pb    ),
    .pr    ( pr    )
);

assign VGA_HS = scandoubler_disable ? csync : (ypbpr ? !(HS_sd_out ^ VS_sd_out) : HS_sd_out);
assign VGA_VS = (scandoubler_disable || ypbpr) ? 1'd1 : VS_sd_out;
assign VGA_R = ypbpr?pr:R_out;
assign VGA_G = ypbpr? y:G_out;
assign VGA_B = ypbpr?pb:B_out;

////////////////////  SOUND //////////////////////
wire [7:0] psg_out;
wire       psg_sel = ~nIORQ & &addr[3:0]; //xF
wire [7:0] psg_ch_a, psg_ch_b, psg_ch_c;

YM2149 psg
(
    .CLK(clk_sys),
    .CE(ce_psg),
    .RESET(reset),
    .BDIR(psg_sel & ~nWR),
    .BC(psg_sel & (&addr[7:6] ^ nWR)),
    .DI(cpu_dout),
    .DO(psg_out),
    .CHANNEL_A(psg_ch_a),
    .CHANNEL_B(psg_ch_b),
    .CHANNEL_C(psg_ch_c)
);

// Route vsync through a high-pass filter to filter out sync signals from the
// tape audio
wire [7:0] mic_out;
wire       mic_bit = mic_out > 8'd8 && mic_out < 8'd224;

rc_filter_1o #(
    .R_ohms_g(33000),
    .C_p_farads_g(47000),
    .fclk_hz_g(6500000),
    .cwidth_g(18)) mic_filter
(
    .clk_i(clk_sys),
    .clken_i(ce_65),
    .res_i(reset),
    .din_i({1'b0, vsync, 6'd0 }),
    .dout_o(mic_out)
);

wire [8:0] audio_l = { 1'b0, psg_ch_a } + { 1'b0, psg_ch_c } + { mic_bit, 4'd0 };
wire [8:0] audio_r = { 1'b0, psg_ch_b } + { 1'b0, psg_ch_c } + { mic_bit, 4'd0 };

sigma_delta_dac #(7) dac_l
(
    .CLK(clk_sys),
    .RESET(reset),
    .DACin(audio_l[8:1]),
    .DACout(AUDIO_L)
);

sigma_delta_dac #(7) dac_r
(
    .CLK(clk_sys),
    .RESET(reset),
    .DACin(audio_r[8:1]),
    .DACout(AUDIO_R)
);
////////////////////   HID   /////////////////////

wire kbd_n = nIORQ | nRD | addr[0];

wire [11:1] Fn;
wire  [2:0] mod;
wire  [4:0] key_data;

keyboard kbd( .* );


wire kbd_intr;
reg [1:0] kbd_edge;
wire [7:0] kbd_scancode;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( ce_65 ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

reg key_pressed;

always @(posedge clk_sys)
begin
    kbd_edge <= {kbd_edge[0],kbd_intr};

    if (kbd_edge == 2'b01)
    begin

        key_pressed <= 1'b1;
  
        if (kbd_scancode == 8'hF0)
            begin
              key_pressed <= 1'b0;
            end
        else
            begin 
                ps2_key[7:0] <=  kbd_scancode;
                ps2_key[10] <= ~ps2_key[10];
            end

       ps2_key[9] <= key_pressed;

    end
end


endmodule
