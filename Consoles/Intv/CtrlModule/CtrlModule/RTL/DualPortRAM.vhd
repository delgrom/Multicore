--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- Generic dual-port RAM implementation -
-- will hopefully work for both Altera and Xilinx parts

library ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;


ENTITY DualPortRAM IS
	GENERIC
	(
		addrbits : integer := 11;
		databits : integer := 18
	);
	PORT
	(
		address_a		: IN STD_LOGIC_VECTOR (addrbits-1 downto 0);
		address_b		: IN STD_LOGIC_VECTOR (addrbits-1 downto 0);
		clock		: IN STD_LOGIC  := '1';
		data_a		: IN STD_LOGIC_VECTOR (databits-1 downto 0);
		data_b		: IN STD_LOGIC_VECTOR (databits-1 downto 0);
		wren_a		: IN STD_LOGIC  := '0';
		wren_b		: IN STD_LOGIC  := '0';
		q_a		: OUT STD_LOGIC_VECTOR (databits-1 downto 0);
		q_b		: OUT STD_LOGIC_VECTOR (databits-1 downto 0)
	);
END DualPortRAM;

architecture arch of DualPortRAM is

type ram_type is array(natural range ((2**addrbits)-1) downto 0) of std_logic_vector(databits-1 downto 0);
shared variable ram : ram_type;

begin

-- Port A
process (clock)
begin
	if (clock'event and clock = '1') then
		if wren_a='1' then
			ram(to_integer(unsigned(address_a))) := data_a;
			q_a <= data_a;
		else
			q_a <= ram(to_integer(unsigned(address_a)));
		end if;
	end if;
end process;

-- Port B
process (clock)
begin
	if (clock'event and clock = '1') then
		if wren_b='1' then
			ram(to_integer(unsigned(address_b))) := data_b;
			q_b <= data_b;
		else
			q_b <= ram(to_integer(unsigned(address_b)));
		end if;
	end if;
end process;


end architecture;
