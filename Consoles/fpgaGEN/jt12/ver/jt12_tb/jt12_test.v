/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns / 1ps

module jt12_test;

reg	rst;

`include "../common/dump.vh"


reg clk; // 4MHz clock

initial begin
	clk = 0;
    forever #125 clk=~clk;
end

reg [1:0] clkcnt;
reg clk_en;

reg rst0;

initial begin
	rst0=0;
    #10 rst0=1;
    #10 rst0=0;
end


always @(negedge clk or posedge rst0)
	if( rst0 ) begin
    	clkcnt <= 2'd0;
    	clk_en <= 1'b0;
    end
    else begin
    	if ( clkcnt== 2'b1 ) begin
        	clkcnt <= 2'd0;
        	clk_en <= 1'b1;
        end
        else begin
        	clkcnt <= clkcnt+1'b1;
        	clk_en <= 1'b0;
        end
    end

integer limit_time_cnt;

initial begin
	rst = 0;
    limit_time_cnt=0;
    #500 rst = 1;
    #600 rst = 0;
	// reset again, when all the pipeline is clear
	#(2500*1000) rst=1;
	#1000 rst=0;
end

`ifdef LIMITTIME
initial begin
    for( limit_time_cnt=`LIMITTIME; limit_time_cnt>0; limit_time_cnt=limit_time_cnt-1 )
		#(1000*1000);
	$finish;
end
`endif


wire	cs_n, wr_n, prog_done;
wire	[ 7:0]	din, dout;
wire signed	[11:0]	right, left;
wire	[ 1:0]	addr;

jt12_testdata #(.rand_wait(`RANDWAIT)) u_testdata(
	.rst	( rst	),
	.clk	( clk	),
	.cs_n	( cs_n	),
	.wr_n	( wr_n	),
	.dout	( din	),
	.din	( dout	),
	.addr	( addr	),
	.prog_done(prog_done)
);

always @(posedge clk)
	if( prog_done ) begin
    	#(2000*1000);
        `ifdef DUMPSOUND
        $display("DUMP END");
        `endif
        $finish;
     end

wire	sample, mux_sample;
wire signed [11:0] snd_left, snd_right;

wire irq_n = 1'b1;

jt12 uut(
	.rst		( rst	),
	.clk		( clk	),
	.cen		( 1'b1	),
	.din		( din	),
	.addr		( addr	),
	.cs_n		( cs_n	),
	.wr_n		( wr_n	),

	.limiter_en( 1'b1 ),

	.dout		( dout	),
	.irq_n		( irq_n	),
	// 1 bit output per channel at 1.3MHz
	.snd_left	( snd_left	),
	.snd_right	( snd_right	),
	// unused outputs
	.snd_sample(),
	.mux_right(),
	.mux_left(),
	.mux_sample()
);

`ifdef DUMPSOUND
initial $display("DUMP START");
`endif

endmodule
