/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns / 1ps


/* This file is part of JT12.

 
	JT12 program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	JT12 program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with JT12.  If not, see <http://www.gnu.org/licenses/>.

	Author: Jose Tejada Gomez. Twitter: @topapate
	Version: 1.0
	Date: 14-2-2017	

*/

module jt12_clk(
	input		rst,
	input		clk,
	
	input		set_n6,
	input		set_n3, // not implemented because it relies on 50% duty cycle of clk
	input		set_n2,
	
	output	reg clk_int,
	output	reg rst_int
);

reg	clk_n2, clk_n6;
wire clk_n3;

reg [1:0] clksel;

parameter USE6=2'd0, USE3=2'b10, USE2=2'b01;

always @(negedge clk_n6 or posedge rst_int) 
	if( rst_int ) begin
		clksel <= USE6;
	end
	else
		clksel <= { set_n3, set_n2 };	

always @(*)
	case( clksel )
		USE3: clk_int <= clk_n3;
		USE2: clk_int <= clk_n2;
		default: clk_int <= clk_n6;
	endcase

// n=2
// Generate internal clock and synchronous reset for it.
reg	[1:0] rst_int_aux;

always @(posedge clk or posedge rst) 
	clk_n2 	<= rst ? 1'b0 : ~clk_n2;
	
reg [1:0] cnt3;
reg [2:0] cnt6;
assign clk_n3 = cnt3[0];

always @(posedge clk or posedge rst) 
	if( rst ) begin
		cnt3 <= 2'd0;
		cnt6 <= 3'd0;
		clk_n6 <= 1'b0;
	end else begin
		cnt3 <= cnt3==2'd2 ? 2'd0 : cnt3+2'd1;
		cnt6 <= cnt6==3'd5 ? 3'd0 : cnt6+3'd1;
		clk_n6 <= cnt6<=3'd2;
	end

wire [3:0] delay;

always @(posedge clk_n6 or posedge rst) 
	if( rst ) begin
		rst_int_aux	<= 2'b11;
		rst_int		<= 1'b1;
	end
	else begin		
		{ rst_int, rst_int_aux } <= { rst_int_aux, 1'b0 };
	end

	
endmodule
