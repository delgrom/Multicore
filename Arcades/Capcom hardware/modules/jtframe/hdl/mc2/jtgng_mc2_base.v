/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//*  This file is part of JT_GNG.
    JT_GNG program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JT_GNG program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JT_GNG.  If not, see <http://www.gnu.org/licenses/>.

    Author: Jose Tejada Gomez. Twitter: @topapate
    Version: 1.0
    Date: 27-10-2017 
	 
	 Multicore 2 port by Victor Trucco
		 
	 */

`timescale 1ns/1ps
`default_nettype none

module jtgng_mc2_base(
    input           rst,
    input           clk_sys,
    input           clk_rom,
    input           clk_vga,
    input           clk_40,
    input           SDRAM_CLK,      // SDRAM Clock
    output          osd_shown,

    // Base video
    input   [1:0]   osd_rotate,
    input   [3:0]   game_r,
    input   [3:0]   game_g,
    input   [3:0]   game_b,
    input           LHBL,
    input           LVBL,
    input           hs,
    input           vs, 
    input           pxl_cen,
	 
    // Scan-doubler video
    input   [5:0]   scan2x_r,
    input   [5:0]   scan2x_g,
    input   [5:0]   scan2x_b,
    input           scan2x_hs,
    input           scan2x_vs,
    output          scan2x_enb, // scan doubler enable bar = scan doubler disable.
	 
    // Final video: VGA+OSD or base+OSD depending on configuration
    output  [4:0]   VIDEO_R,
    output  [4:0]   VIDEO_G,
    output  [4:0]   VIDEO_B,
    output          VIDEO_HS,
    output          VIDEO_VS,
	 
    // SPI interface to arm io controller
    input           SPI_DI,
    input           SPI_SCK,
    input           SPI_SS2,

    // control
    output [31:0]   status,

	 
    // Sound
    input           clk_dac,
    input   [15:0]  snd,
    output          snd_pwm
);

parameter CONF_STR="CORE";
parameter CONF_STR_LEN=4;
parameter SIGNED_SND=1'b0;


`ifndef SIMULATION
`ifndef NOSOUND
wire [15:0] snd_in = {snd[15]^SIGNED_SND, snd[14:0]};
wire [19:0] snd_padded = { 1'b0, snd_in, 3'd0 };
//wire [19:0] snd_padded = { snd_in, 4'd0 };

//dac #(.msbi_g (19))
//u_dac 
//(
//    .clk_i   ( clk_dac    ),
//    .res_n_i ( ~rst       ),
//    .dac_i   ( snd_padded ),
//    .dac_o   ( snd_pwm    )
//);


hifi_1bit_dac u_dac
(
 .reset    ( rst        ),
 .clk      ( clk_dac    ),
 .clk_ena  ( 1'b1       ),
 .pcm_in   ( snd_padded ),
 .dac_out  ( snd_pwm    )
);

`endif
`else
assign snd_pwm = 1'b0;
`endif
assign status    = 32'd0;
// OSD will only get simulated if SIMULATE_OSD is defined
`ifndef SIMULATE_OSD
`ifndef SCANDOUBLER_DISABLE
`ifdef SIMULATION
`define BYPASS_OSD
`endif
`endif
`endif

`ifdef SIMINFO
initial begin
    $display("INFO: use -d SIMULATE_OSD to simulate the MiST OSD")
end
`endif


`ifndef BYPASS_OSD
wire       HSync = scan2x_enb ? ~hs : scan2x_hs;
wire       VSync = scan2x_enb ? ~vs : scan2x_vs;
wire       CSync = ~(HSync ^ VSync);
assign scan2x_enb = 1'b0; // 0 = scandoubler always enabled

assign VIDEO_R  = (scan2x_enb) ? { game_r, game_r[3] } : scan2x_r[5:1];
assign VIDEO_G  = (scan2x_enb) ? { game_g, game_g[3] } : scan2x_g[5:1];
assign VIDEO_B  = (scan2x_enb) ? { game_b, game_b[3] } : scan2x_b[5:1];


// a minimig vga->scart cable expects a composite sync signal on the VIDEO_HS output.
// and VCC on VIDEO_VS (to switch into rgb mode)
assign VIDEO_HS = ( scan2x_enb ) ? CSync : HSync;
assign VIDEO_VS = ( scan2x_enb ) ? 1'b1 : VSync;
`else
assign VIDEO_R  = game_r;// { game_r, game_r[3:2] };
assign VIDEO_G  = game_g;// { game_g, game_g[3:2] };
assign VIDEO_B  = game_b;// { game_b, game_b[3:2] };
assign VIDEO_HS = hs;
assign VIDEO_VS = vs;
`endif

endmodule // jtgng_mist_base