/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns / 1ps

module test;

reg rst_n, clk, start;
reg  [15:0] a, b;
wire [15:0] d,r;

initial begin
    rst_n = 1'b1;
    #5 rst_n = 1'b0;
    #7 rst_n = 1'b1;
end

initial begin
    clk = 1'b0;
    #10;
    forever #10 clk = ~clk;
end

wire [15:0] check = b*d+r;

initial begin
    a = 16'd1235;
    b = 16'd23;
    start = 0;
    #20;
    start = 1;
    #40 start = 0;

    #(36*20);
    a = 16'd3235;
    b = 16'd123;
    start = 0;
    #20;
    start = 1;
    #40 start = 0;

    #(36*20);
    a = 16'd32767;
    b = 16'd1;
    start = 0;
    #20;
    start = 1;
    #40 start = 0;

    #(36*20);
    a = 16'd100;
    b = 16'd1000;
    start = 0;
    #20;
    start = 1;
    #40 start = 0;

    #(36*20);
    a = 16'd28000;
    b = 16'd14000;
    start = 0;
    #20;
    start = 1;
    #40 start = 0;

    $finish;
end

jt10_adpcm_div #(.dw(16))uut(
    .rst_n  ( rst_n     ),
    .clk    ( clk       ),
    .cen    ( 1'b1      ),
    .start  ( start     ),
    .a      ( a     ),
    .b      ( b     ),
    .d      ( d     ),
    .r      ( r     )
);

`ifdef NCVERILOG
initial begin
    $shm_open("test.shm");
    $shm_probe(test,"AS");
end
`else 
initial begin
    $dumpfile("test.fst");
    $dumpvars;
end
`endif

endmodule // test