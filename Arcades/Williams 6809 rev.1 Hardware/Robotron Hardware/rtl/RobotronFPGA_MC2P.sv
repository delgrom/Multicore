//============================================================================
//  Robotron-FPGA MiST top-level
//
//  Robotron-FPGA is Copyright 2012 ShareBrained Technology, Inc.
//
//  Supports:
//  Robotron 2048/Joust/Splat/Bubbles/Stargate/Alien Arena/Sinistar/
//  Playball!/Lotto Fun/Speed Ball

//============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none

module RobotronFPGA_MC2P
(
    // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT         = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                      = 1'b1 // '0' is LED on
);


`define CORE_NAME "ROBOTRON"

localparam CONF_STR = {
    "P,CORE_NAME.dat;",
    "S,DAT,Alternative ROM...;", 
    "O12,Screen Rotate,0,90,180,270;",
    "O34,Scanlines,Off,25%,50%,75%;",
    "O5,Blend,Off,On;",
    "O8,Scandoubler,On,Off;",
//    "O67,Control,Mode 1,Mode 2,Cabinet;",
    "T0,Reset;"
};


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clk_sys ),
    
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_sys, ~pll_locked, ioctl_downl, pump_s);

//-- END defaults -------------------------------------------------------


wire [1:0] scanlines = status[4:3];
wire       blend     = status[5];
wire       autoup    = 1'b0; //status[7];
wire       adv       = 1'b0; //status[8];

reg   [7:0] SW;
reg   [7:0] JA;
reg   [7:0] JB;
reg   [7:0] AN0;
reg   [7:0] AN1;
reg   [7:0] AN2;
reg   [7:0] AN3;
reg   [3:0] BTN;
reg         blitter_sc2, sinistar, speedball;
reg         speech_en;

wire  [6:0] core_mod;
reg   [1:0] orientation; // [left/right, landscape/portrait]

// advance button
reg  [23:0] adv_counter;
wire        advance = (adv_counter != 0);

always @(posedge clk_sys) begin
    reg adv_d;

    adv_d <= adv;
    if (~adv_d & adv) adv_counter <= 24'hfffff;
    if (adv_counter != 0) adv_counter <= adv_counter - 1'd1;
end

always @(*) begin
    orientation = 2'b10;
    SW = {6'h0, advance, autoup};
    JA = 8'hFF;
    JB = 8'hFF;
    BTN = 4'hF;
    AN0 = 8'h80;
    AN1 = 8'h80;
    AN2 = 8'h80;
    AN3 = 8'h80;
    blitter_sc2 = 0;
    sinistar = 0;
    speedball = 0;
    speech_en = 0;

    case (core_mod)
    7'h0: // ROBOTRON
    begin
        BTN = { btn_one_player, btn_two_players, btn_coin | m_coin2, reset };
        // Fire Up/Down/Left/Right maps to joystick 1/2/3/4 and keyboard R/F/D/G (MAME style)
        JA  = ~{ m_fireB|m_right2, m_fireC|m_left2, m_fireA|m_down2, m_fireE|m_fireF|m_fireG|m_up2, m_right, m_left, m_down, m_up };
        JB  = ~{ m_fireB|m_right2, m_fireC|m_left2, m_fireA|m_down2, m_fireE|m_fireF|m_fireG|m_up2, m_right, m_left, m_down, m_up };
    end
    7'h1: // JOUST
    begin
        BTN = { btn_two_players, btn_one_player, btn_coin | m_coin2, reset };
        JA  = ~{ 5'b00000, m_fireA, m_right, m_left };
        JB  = ~{ 5'b00000, m_fire2A, m_right2, m_left2 };
    end
    7'h2: // SPLAT
    begin
        blitter_sc2 = 1;
        BTN = { btn_one_player, btn_two_players, btn_coin | m_coin2, reset };
        // Fire Up/Down/Left/Right maps to joystick 1/2/3/4 and keyboard R/F/D/G (MAME style)
        JA  = ~{ m_fireB|m_right2, m_fireC|m_left2, m_fireA|m_down2, m_fireE|m_fireF|m_fireG|m_up2, m_right, m_left, m_down, m_up };
        JB  = ~{ m_fireB|m_right2, m_fireC|m_left2, m_fireA|m_down2, m_fireE|m_fireF|m_fireG|m_up2, m_right, m_left, m_down, m_up };
    end
    7'h3: // BUBBLES
    begin
        BTN = { btn_two_players, btn_one_player, btn_coin | m_coin2, reset };
        JA  = ~{ 4'b0000, m_right, m_left, m_down, m_up };
        JB  = ~{ 4'b0000, m_right2, m_left2, m_down2, m_up2 };
    end
    7'h4: // STARGATE
    begin
        BTN = { btn_two_players, btn_one_player, btn_coin | m_coin2, reset };
        JA  = ~{ m_fireE, m_up, m_down, m_left | m_right, m_fireD, m_fireC, m_fireB, m_fireA };
        JB  = ~{ m_fire2E, m_up2, m_down2, m_left2 | m_right2, m_fire2D, m_fire2C, m_fire2B, m_fire2A };
    end
    7'h5: // ALIENAR
    begin
        BTN = { btn_one_player, btn_two_players, btn_coin | m_coin2, reset };
        JA  = ~{ 1'b0, 1'b0, m_fireB, m_fireA, m_right, m_left, m_down, m_up };
        JB  = ~{ 1'b0, 1'b0, m_fire2B, m_fire2A, m_right2, m_left2, m_down2, m_up2 };
    end
    7'h6: // SINISTAR
    begin
        sinistar = 1;
        speech_en = 1;
        orientation = 2'b01;
        BTN = { btn_two_players, btn_one_player, btn_coin | m_coin2, reset };
        JA  = { sin_x, 2'b00, m_right | m_left | m_right2 | m_left2, sin_y, 2'b00, m_up | m_down | m_up2 | m_down2 };
        JB  = { sin_x, 2'b00, m_right | m_left | m_right2 | m_left2, sin_y, 2'b00, m_up | m_down | m_up2 | m_down2 };
    end
    7'h7: // PLAYBALL
    begin
        speech_en = 1;
        orientation = 2'b01;
        BTN = { 2'b00, btn_coin | m_coin2, reset };
        JA  = ~{ 4'b0000, btn_two_players, m_right, m_left, btn_one_player };
        JB  = JA;
    end
    7'h8: // LOTTO FUN
    begin
        BTN = { btn_one_player | m_fireA, 1'b0, btn_coin | m_coin2, reset };
        JA  = ~{ 4'b0000, m_right, m_left, m_down, m_up };
        JB  = 8'b11111111;//IN1
    end
    7'h9: // SPEED BALL
    begin
        speedball = 1;
        BTN = { btn_two_players, btn_one_player, btn_coin | m_coin2, reset };
        JA  = { m_fireD, m_fireC, m_fireB, m_fireA | mouse_btns0[0], m_right, m_left, m_down, m_up };
        JB  = { m_fire2D, m_fire2C, m_fire2B, m_fire2A | mouse_btns1[0], m_right2, m_left2, m_down2, m_up2 };
        AN0 = {~y_pos0[8], y_pos0[7:1]};
        AN1 = {~x_pos0[8], x_pos0[7:1]};
        AN2 = {~y_pos1[8], y_pos1[7:1]};
        AN3 = {~x_pos1[8], x_pos1[7:1]};
    end
    default: ;
    endcase
end

reg  signed [9:0] x_pos0;
reg  signed [9:0] y_pos0;
reg  [1:0] mouse_btns0;

reg  signed [9:0] x_pos1;
reg  signed [9:0] y_pos1;
reg  [1:0] mouse_btns1;

always @(posedge clk_sys) begin
  if (mouse_strobe) begin
        if (~mouse_idx) begin
            mouse_btns0 <= mouse_flags[1:0];
            x_pos0 <= x_pos0 + mouse_x;
            y_pos0 <= y_pos0 + mouse_y;
        end else begin
            mouse_btns1 <= mouse_flags[1:0];
            x_pos1 <= x_pos1 + mouse_x;
            y_pos1 <= y_pos1 + mouse_y;
        end
  end
end

assign LED = ~ioctl_downl;
//assign SDRAM_CLK = clk_mem;
assign SDRAM_CKE = 1;

wire clk_sys, clk_mem, clk_aud;
wire pll_locked;

pll_mist pll(
    .inclk0(clock_50_i),
    .areset(0),
    .c0(clk_mem),//96
    .c1(clk_sys),//12
    .c2(clk_aud),//0.89
    .c3(SDRAM_CLK),
    .locked(pll_locked)
    );

wire clk_25m2,clk_40;    
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        no_csync;
wire        ypbpr;
wire        key_pressed;
wire  [7:0] key_code;
wire        key_strobe;

wire        mouse_strobe;
wire signed [8:0] mouse_x;
wire signed [8:0] mouse_y;
wire  [7:0] mouse_flags;
wire        mouse_idx;

/*
user_io #(
    .STRLEN($size(CONF_STR)>>3))
user_io(
    .clk_sys        ( clk_sys          ),
    .conf_str       ( CONF_STR         ),
    .SPI_CLK        ( SPI_SCK          ),
    .SPI_SS_IO      ( CONF_DATA0       ),
    .SPI_MISO       ( SPI_DO           ),
    .SPI_MOSI       ( SPI_DI           ),
    .buttons        ( buttons          ),
    .switches       ( switches         ),
    .scandoubler_disable (scandoublerD ),
    .ypbpr          ( ypbpr            ),
    .no_csync       ( no_csync         ),
    .core_mod       ( core_mod         ),
    .key_strobe     ( key_strobe       ),
    .key_pressed    ( key_pressed      ),
    .key_code       ( key_code         ),
    .mouse_idx      ( mouse_idx        ),
    .mouse_strobe   ( mouse_strobe     ),
    .mouse_x        ( mouse_x          ),
    .mouse_y        ( mouse_y          ),
    .mouse_flags    ( mouse_flags      ),
    .joystick_0     ( joystick_0       ),
    .joystick_1     ( joystick_1       ),
    .status         ( status           )
    );
*/

wire        ioctl_downl;
wire        ioctl_upl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;
wire  [7:0] ioctl_din;

/*
ROM Structure:
00000-0BFFF main cpu 48k
0C000-0CFFF snd  cpu  4k
0D000-0D3FF CMOS RAM  1k
0D400-0DFFF ---
0E000-11FFF speech   16k
*/

data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_mem      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),

    .data_in       ( pump_s & keys_s ),
    .conf_str      ( CONF_STR    ),
    .status        ( status      ),
    .core_mod      ( core_mod    ),

    .ioctl_download( ioctl_downl  ),
//    .ioctl_upload  ( ioctl_upl    ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
//    .ioctl_din     ( ioctl_din    )
);

reg         port1_req;
wire [15:0] port1_do;
wire [23:1] mem_addr;
wire [15:0] mem_do;
wire [15:0] mem_di;
wire        mem_oe;
wire        mem_we;
wire        ramcs;
wire        romcs;
wire        ramlb;
wire        ramub;

reg         clkref;
always @(posedge clk_sys) clkref <= ~clkref;

sdram #(.MHZ(96)) sdram(
    .*,
    .init_n        ( pll_locked   ),
    .clk           ( clk_mem      ),
    .clkref        ( clkref       ),

    // ROM upload
    .port1_req     ( port1_req    ),
    .port1_ack     ( ),
    .port1_a       ( downl_addr ),
    .port1_ds      ( 2'b11 ),
    .port1_we      ( ioctl_downl ),
    .port1_d       ( {ioctl_dout[7:4], ioctl_dout[7:4], ioctl_dout[3:0], ioctl_dout[3:0]} ),
    .port1_q       ( port1_do ),

    // CPU/video access
    .cpu1_addr     ( ioctl_downl ? 17'h1ffff : sdram_addr ),
    .cpu1_d        ( mem_di  ),
    .cpu1_q        ( mem_do  ),
    .cpu1_oe       ( ~mem_oe & ~(ramcs & romcs) ),
    .cpu1_we       ( ~mem_we & ~(ramcs & romcs) ),
    .cpu1_ds       ( ~romcs ? 2'b11 : ~{ramub, ramlb} )
);

// ROM address to SDRAM address:
// 0xxx-8xxx -> 0xxx-8xxx
// DXXX-FXXX -> 9xxx-Bxxx
wire [17:1] sdram_addr = ~romcs ? {1'b0, mem_addr[16], ~mem_addr[16] & mem_addr[15], mem_addr[14:1]} : { 1'b1, mem_addr[16:1] };

// IOCTL address to SDRAM address:
// D000-D3FF (ROM) or 000-3FFF (RAM) -> 1CC00-1CFFF (CMOS), otherwise direct mapping

wire [22:0] downl_addr = 
  ((ioctl_index == 0 && ioctl_addr[22:10] == { 7'h0, 4'hD, 2'b00 }) || ioctl_index == 8'hff) ? { 1'b1, 4'hC, 2'b11, ioctl_addr[9:0] } :
    ioctl_addr[22:0];

assign ioctl_din = { port1_do[11:8], port1_do[3:0] };

always @(posedge clk_mem) begin
    reg        ioctl_wr_last = 0;
    reg  [9:0] cmos_addr;
    reg        ioctl_upl_d;

    ioctl_wr_last <= ioctl_wr;
    if (ioctl_downl) begin
        if (~ioctl_wr_last && ioctl_wr) begin
            port1_req <= ~port1_req;
        end
    end

    ioctl_upl_d <= ioctl_upl;
    cmos_addr <= ioctl_addr[9:0];
    if (ioctl_upl) begin
        if (cmos_addr != ioctl_addr[9:0] || !ioctl_upl_d) begin
            port1_req <= ~port1_req;
        end
    end
end

reg reset = 1;
reg rom_loaded = 0;
always @(posedge clk_sys) begin
    reg ioctl_downlD;
    ioctl_downlD <= ioctl_downl;

    if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
    reset <= status[0] | ~btn_n_o[4] | ioctl_downl | ~rom_loaded;
end

wire  [7:0] audio;
wire [15:0] speech;
wire        hs, vs, hb, vb;
wire  [2:0] r,g;
wire  [1:0] b;

robotron_soc robotron_soc (
    .clock       ( clk_sys     ),
    .clock_snd   ( clk_aud     ),
    .vgaRed      ( r           ),
    .vgaGreen    ( g           ),
    .vgaBlue     ( b           ),
    .Hsync       ( hs          ),
    .Vsync       ( vs          ),
//    .HB          ( hb          ),
//    .VB          ( vb          ),
    .audio_out   ( audio       ),
    .speech_out  ( speech      ),

    .blitter_sc2 ( blitter_sc2 ),
    .sinistar    ( sinistar    ),
    .speedball   ( speedball   ),
    .pause       ( ioctl_upl   ),
    .BTN         ( BTN         ),
    .SIN_FIRE    ( ~m_fireA & ~m_fire2A ),
    .SIN_BOMB    ( ~m_fireB & ~m_fire2B ),
    .SW          ( SW          ),
    .JA          ( JA          ),
    .JB          ( JB          ),
    .AN0         ( AN0         ),
    .AN1         ( AN1         ),
    .AN2         ( AN2         ),
    .AN3         ( AN3         ),

    .MemAdr      ( mem_addr    ),
    .MemDin      ( mem_di      ),
    .MemDout     ( mem_do      ),
    .MemOE       ( mem_oe      ),
    .MemWR       ( mem_we      ),
    .RamCS       ( ramcs       ),
    .RamLB       ( ramlb       ),
    .RamUB       ( ramub       ),
    .FlashCS     ( romcs       ),

    .dl_clock    ( clk_mem  ),
    .dl_addr     ( ioctl_addr[16:0] ),
    .dl_data     ( ioctl_dout ),
    .dl_wr       ( ioctl_wr && ioctl_index == 0 )
);


wire [16:0] audio_mix = speech_en ? { 1'b0, audio, audio } + { 1'b0, speech } : { 1'b0, audio, audio };
wire dac_o;
assign AUDIO_L = dac_o;
assign AUDIO_R = dac_o;

dac #(
    .C_bits(17))
dac(
    .clk_i(clk_aud),
    .res_n_i(1),
    .dac_i(audio_mix),
    .dac_o(dac_o)
    );

// Sinistar controls
reg sin_x;
reg sin_y;

always @(posedge clk_sys) begin
    if (m_right | m_right2) sin_x <= 0;
    else if (m_left | m_left2) sin_x <= 1;

    if (m_up | m_up2) sin_y <= 0;
    else if (m_down | m_down2) sin_y <= 1;
end

///////////////////////////////////////////////////////////////////

wire ce_pix = pcnt[0];

reg        HBlank;
reg        VBlank;
reg [10:0] lcnt;
reg [10:0] pcnt;

always @(posedge clk_sys) begin
    reg old_vs, old_hs;

    if(~&pcnt) pcnt <= pcnt + 1'd1;

    old_hs <= hs;
    if(~old_hs & hs) begin
        pcnt <= 0;
        if(~&lcnt) lcnt <= lcnt + 1'd1;

        old_vs <= vs;
        if(~old_vs & vs) lcnt <= 0;
    end

    if (pcnt[10:1] == 336) HBlank <= 1;
    if (pcnt[10:1] == 040) HBlank <= 0;

    if (lcnt == 246) VBlank <= 1;
    if (lcnt == 006) VBlank <= 0;
end
///////////////////////////////////////////////////////////////////


wire [10:0] vga_col_s;
wire vga_hs_s, vga_vs_s;
wire blankn = ~(HBlank | VBlank);

framebuffer #(292,240,8,0,1) framebuffer
(
        .clk_sys    ( clk_sys ),
        .clk_i      ( ce_pix ),
        .RGB_i      ((blankn) ? {r,g,b} : 8'b00000000 ),
        .hblank_i   ( HBlank ),
        .vblank_i   ( VBlank ),
        .dis_db_i   ( 1 ),
        .rotate_i   ( status[2:1] ), 

        .clk_vga_i  ( (status[1]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);

wire direct_video_s = ~status[8] ^ direct_video;

mist_video #(.COLOR_DEPTH(3), .SD_HCNT_WIDTH(11),.USE_FRAMEBUFFER(1)) mist_video(
    .clk_sys        ( direct_video_s ? clk_sys :(status[1]) ? clk_40 : clk_25m2          ),
    .SPI_SCK        ( SPI_SCK          ),
    .SPI_SS3        ( SPI_SS2          ),
    .SPI_DI         ( SPI_DI           ),

    .R       ( (direct_video_s) ? (blankn) ? r : 3'b000 : vga_col_s[7:5] ),
    .G       ( (direct_video_s) ? (blankn) ? g : 3'b000 : vga_col_s[4:2]  ),
    .B       ( (direct_video_s) ? (blankn) ? {b,b[1]} : 3'b000 : {vga_col_s[1:0],vga_col_s[0]} ),
    .HSync   ( (direct_video_s) ? hs : vga_hs_s ),
    .VSync   ( (direct_video_s) ? vs : vga_vs_s ),

    .VGA_R          ( VGA_R            ),
    .VGA_G          ( VGA_G            ),
    .VGA_B          ( VGA_B            ),
    .VGA_VS         ( VGA_VS           ),
    .VGA_HS         ( VGA_HS           ),

    .ce_divider     ( 1'b1 ),
    .rotate         ( osd_rotate ),
    .scandoubler_disable( direct_video_s ),
    .no_csync       ( ~direct_video_s  ),
    .scanlines      ( status[4:3]        ),
    .blend          ( status[5]       ),
    .osd_enable     ( osd_enable       )
    );


wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_o[1] | m_one_player;
wire btn_two_players = ~btn_n_o[2] | m_two_players;
wire btn_coin        = ~btn_n_o[3] | m_coin1;
wire [7:0] keys_s;
wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire direct_video;
wire [1:0]osd_rotate;
wire [4:1]btn_n_o;

//translate scancode to joystick
MC2_HID #( .OSD_CMD( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(12000)) k_hid
(
    .clk          ( clk_sys ),
    .kbd_clk      ( ps2_clk_io ),
    .kbd_dat      ( ps2_data_io ),
    .joystick_0   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer    ( 0 ),

    //-- tilt, coin4-1, start4-1
    .controls     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    .direct_video ( direct_video ),
    .osd_rotate   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o        ( keys_s ),
    .osd_enable   ( osd_enable ),
    
    //-- sega joystick
    .sega_strobe  ( joy_p7_o ),

     //-- Front buttons
     .front_buttons_i (btn_n_i),
     .front_buttons_o (btn_n_o)        
);


endmodule 
