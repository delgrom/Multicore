//============================================================================
//  Arcade: Defender
//
//  Port to MiSTer
//  Copyright (C) 2017 Sorgelig
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

//============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none

module Defender_MC2P(
    // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT         = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                      = 1'b1 // '0' is LED on
);

localparam CONF_STR = {
    "P,CORE_NAME.dat;",
//    "P,Defender.dat;",
    "S,DAT,Alternative ROM...;", 
    "O12,Screen Rotate,0,90,180,270;",
    "O34,Scanlines,Off,25%,50%,75%;",
    "O5,Blend,Off,On;",
    "O8,Scandoubler,On,Off;",
    "O67,Control,Mode 1,Mode 2,Cabinet;",
    "O9,Autoup,Off,On;",
    "T0,Reset;"


};

//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//no SDRAM for this core
assign SDRAM_nCS = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clk_sys ),
    
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_sys, ~pll_locked, ioctl_download, pump_s);

//-- END defaults -------------------------------------------------------

////////////////////   CLOCKS   ///////////////////

wire clk_sys, clk_6, clk_48;
wire pll_locked;

pll pll
(
    .inclk0(clock_50_i),
    .c0(clk_48),  // 48
    .c1(clk_sys), // 24
    .c2(clk_6),    // 6
    .locked(pll_locked)
);

wire clk_25m2,clk_40;    
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

///////////////////////////////////////////////////

wire [31:0] status;
wire  [1:0] buttons;
wire        forced_scandoubler;

wire        ioctl_download;
wire        ioctl_upload;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;
wire  [7:0] ioctl_din;
wire  [7:0] ioctl_index;
wire  [7:0] ioctl_data;
wire        ioctl_wait;


wire [31:0] joy1, joy2;
wire [31:0] joy = joy1 | joy2;

wire [21:0] gamma_bus;
/*
hps_io #(.STRLEN($size(CONF_STR)>>3)) hps_io
(
    .clk_sys(clk_sys),
    .HPS_BUS(HPS_BUS),

    .conf_str(CONF_STR),

    .buttons(buttons),
    .status(status),
    .status_menumask({mod == mod_defender,landscape,direct_video}),
    .forced_scandoubler(forced_scandoubler),
    .gamma_bus(gamma_bus),
    .direct_video(direct_video),


    .ioctl_download(ioctl_download),
    .ioctl_upload(ioctl_upload),
    .ioctl_wr(ioctl_wr),
    .ioctl_addr(ioctl_addr),
    .ioctl_dout(ioctl_dout),
    .ioctl_din(ioctl_din),

    .ioctl_index(ioctl_index),
    .ioctl_wait(ioctl_wait),

    .joystick_0(joy1),
    .joystick_1(joy2)

);
*/
data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_sys     ),
    .SPI_SCK       ( SPI_SCK     ),
    .SPI_SS2       ( SPI_SS2     ),
    .SPI_DI        ( SPI_DI      ),
    .SPI_DO        ( SPI_DO      ),

    .data_in       ( pump_s & keys_s ),
    .conf_str      ( CONF_STR    ),
    .status        ( status      ),
    .core_mod      ( mod    ),

    .ioctl_download( ioctl_download ),
    .ioctl_index   ( ioctl_index ),
    .ioctl_wr      ( ioctl_wr    ),
    .ioctl_addr    ( ioctl_addr  ),
    .ioctl_dout    ( ioctl_dout  )
);

wire rom_download = ioctl_download && !ioctl_index;

reg reset;
always @(posedge clk_6) reset <= status[0] | ~btn_n_o[4] | rom_download;

///////////////////////////////////////////////////////////////////


localparam mod_defender = 0;
localparam mod_colony7  = 1;
localparam mod_mayday   = 2;
localparam mod_jin      = 3;

reg [6:0] mod = 0;
//always @(posedge clk_sys) if (ioctl_wr & (ioctl_index==1)) mod <= ioctl_dout;

// load the DIPS
reg [7:0] sw[8];
always @(posedge clk_sys) if (ioctl_wr && (ioctl_index==254) && !ioctl_addr[24:3]) sw[ioctl_addr[2:0]] <= ioctl_dout;

///////////////////////////////////////////////////////////////////

reg  [7:0] input0;
reg  [7:0] input1;
reg  [7:0] input2;
reg        mayday;
reg        landscape;
reg        rotate_ccw;
reg        extvbl;

wire autoup = status[9];
wire advance = m_fireG;

always @(posedge clk_sys) begin
    mayday <= 0;

    input0 = { 3'b000, btn_coin, /*btn_score_reset*/1'b0, 1'b0, advance, autoup };

    input1 <= 0;
    input2 <= 0;
    landscape <= 1;
    rotate_ccw <= 0;
    extvbl <= 0;

    case(mod)
    mod_defender:
        begin 
            input1 <= { m_down, (status[7:6]==2'b10)? m_fireE : ( status[7:6]==2'b01 ? (def_state ? m_right : m_left) : (m_left | m_right)), btn_one_player, btn_two_players, m_fireD, m_fireC, status[7:6]==2'b01 ? (def_state ? m_left : m_right) : m_fireB, m_fireA };
            input2 <= { 7'b000000, m_up };
        end
    mod_colony7:
        begin
            landscape  <= 0;
            rotate_ccw <= 1;
            input0 <= { 3'b000, btn_coin, 2'b00, /*bonus at*/status[11], /*lives23*/status[10] };
            input1 <= { m_fireB, m_fireA, btn_one_player, btn_two_players, m_up, m_left, m_right, m_down };
            input2 <= { 7'b000000, m_fireC };
        end
    mod_mayday:
        begin
            mayday <= 1;
            input0 <= { 2'b00, m_coin2, btn_coin, 1'b0, /*btn_score_reset*/1'b0, advance, autoup };
            input1 <= { m_down, 1'b0, btn_one_player, btn_two_players, m_fireB, m_fireC, m_right, m_fireA };
            input2 <= { 7'b000000, m_up };
        end
    mod_jin:
        begin
            landscape <= 0;
            extvbl <= 1;
            input0 <= { 3'b000, m_coin2, btn_coin, 3'b000 };
            input1 <= { m_fireB, m_fireA, btn_one_player, btn_two_players, m_right, m_left, m_down, m_up };
            input2 <= { 1'b0, ~status[12:11], 1'b0, status[10], 3'b000 };
        end
    default:;
    endcase
end

reg [7:0] in0,in1,in2;
reg extvbl1, mayday1;
always @(posedge clk_6) begin
    in0 <= sw[0] | input0;
    in1 <= sw[1] | input1;
    in2 <= sw[2] | input2;
    
    extvbl1 <= extvbl;
    mayday1 <= mayday;
end

///////////////////////////////////////////////////////////////////

wire [2:0] r,g;
wire [1:0] b;
wire HSync, VSync;
wire HBlank, VBlank;
wire def_state;

defender defender
(
    .clock_6(clk_6),
    .reset(reset),
    .defender_state(def_state),

    .dn_clk(clk_sys),
    .dn_addr(ioctl_addr[15:0]),
    .dn_data(ioctl_dout),
    .dn_wr(ioctl_wr & rom_download),
    .dn_nvram_wr(ioctl_wr & (ioctl_index=='d4)), 
    .dn_din(ioctl_din),
    .dn_nvram(ioctl_index=='d4),

    .video_r(r),
    .video_g(g),
    .video_b(b),
    .video_hblank(HBlank),
    .video_vblank(VBlank),
    .video_hs(HSync),
    .video_vs(VSync),
    .audio_out(audio),

    .extvbl(extvbl1),
    .mayday(mayday1),

    .input0(in0),
    .input1(in1),
    .input2(in2)
);

///////////////////////////////////////////////////////////////////

reg ce_pix;
always @(posedge clk_48) begin
    reg [2:0] div;

    div <= div + 1'd1;
    ce_pix <= !div;
end
/*
screen_rotate screen_rotate (.*);

arcade_video #(306,8) arcade_video
(
    .*,
    .clk_video(clk_48),
    .RGB_in({r,g,b}),
    .fx(status[5:3])
);
*/
wire [7:0] audio;
assign AUDIO_L = {audio, audio};
assign AUDIO_R = AUDIO_L;


wire [10:0] vga_col_s;
wire vga_hs_s, vga_vs_s;
wire blankn = ~(HBlank | VBlank);

framebuffer #(292,240,8,0,1) framebuffer
(
        .clk_sys    ( clk_48 ),
        .clk_i      ( ce_pix ),
        .RGB_i      ((blankn) ? {r,g,b} : 8'b00000000 ),
        .hblank_i   ( HBlank ),
        .vblank_i   ( VBlank ),
        .dis_db_i   ( 1 ),
        .rotate_i   ( status[2:1] ), 

        .clk_vga_i  ( (status[1]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);

wire direct_video_s = ~status[8] ^ direct_video;

mist_video #(.COLOR_DEPTH(3), .SD_HCNT_WIDTH(11),.USE_FRAMEBUFFER(1)) mist_video(
    .clk_sys        ( direct_video_s ? clk_48 :(status[1]) ? clk_40 : clk_25m2          ),
    .SPI_SCK        ( SPI_SCK          ),
    .SPI_SS3        ( SPI_SS2          ),
    .SPI_DI         ( SPI_DI           ),

    .R       ( (direct_video_s) ? (blankn) ? r : 3'b000 : vga_col_s[7:5] ),
    .G       ( (direct_video_s) ? (blankn) ? g : 3'b000 : vga_col_s[4:2]  ),
    .B       ( (direct_video_s) ? (blankn) ? {b,b[1]} : 3'b000 : {vga_col_s[1:0],vga_col_s[0]} ),
    .HSync   ( (direct_video_s) ? HSync : vga_hs_s ),
    .VSync   ( (direct_video_s) ? VSync : vga_vs_s ),

    .VGA_R          ( VGA_R            ),
    .VGA_G          ( VGA_G            ),
    .VGA_B          ( VGA_B            ),
    .VGA_VS         ( VGA_VS           ),
    .VGA_HS         ( VGA_HS           ),

    .ce_divider     ( 1'b1 ),
    .rotate         ( osd_rotate ),
    .scandoubler_disable( direct_video_s ),
    .no_csync       ( ~direct_video_s  ),
    .scanlines      ( status[4:3]        ),
    .blend          ( status[5]       ),
    .osd_enable     ( osd_enable       )
    );


wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_o[1] | m_one_player;
wire btn_two_players = ~btn_n_o[2] | m_two_players;
wire btn_coin        = ~btn_n_o[3] | m_coin1;
wire [7:0] keys_s;
wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire direct_video;
wire [1:0]osd_rotate;
wire [4:1]btn_n_o;

//translate scancode to joystick
MC2_HID #( .OSD_CMD( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(24000)) k_hid
(
    .clk          ( clk_sys ),
    .kbd_clk      ( ps2_clk_io ),
    .kbd_dat      ( ps2_data_io ),
    .joystick_0   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer    ( 1 ),

    //-- tilt, coin4-1, start4-1
    .controls     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    .direct_video ( direct_video ),
    .osd_rotate   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o        ( keys_s ),
    .osd_enable   ( osd_enable ),
    
    //-- sega joystick
    .sega_strobe  ( joy_p7_o ),

     //-- Front buttons
     .front_buttons_i (btn_n_i),
     .front_buttons_o (btn_n_o)        
);

endmodule
