/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//*  This file is part of JTBUBL.
    JTBUBL program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JTBUBL program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JTBUBL.  If not, see <http://www.gnu.org/licenses/>.

    Author: Jose Tejada Gomez. Twitter: @topapate
    Version: 1.0
    Date: 29-05-2020 */

module jtbubl_colmix(
    input               clk,
    input               clk24,
    //output              pxl2_cen,
    input               pxl_cen,
    // Screen
    input               LHBL,
    input               LVBL,
    output              LHBL_dly,
    output              LVBL_dly,
    input      [ 7:0]   col_addr,
    // CPU interface
    input               pal_cs,
    output     [ 7:0]   pal_dout,
    input               cpu_rnw,
    input      [ 8:0]   cpu_addr,
    input      [ 7:0]   cpu_dout,
    input               black_n,    
    // Colours
    output     [ 3:0]   red,
    output     [ 3:0]   green,
    output     [ 3:0]   blue
);

wire [15:0] col_out;
wire [15:0] co_bus;
reg  [15:0] col_in;
wire        pal0_we  = pal_cs & ~cpu_rnw & ~cpu_addr[0];
wire        pal1_we  = pal_cs & ~cpu_rnw &  cpu_addr[0];
wire [ 7:0] pal_addr = cpu_addr[8:1];
wire [ 7:0] pal_even, pal_odd;

assign red      = col_out[7:4];
assign green    = col_out[3:0];
assign blue     = col_out[15:12];
assign pal_dout = !cpu_addr[0] ? pal_even : pal_odd;

jtframe_dual_ram #(.aw(8),.simhexfile("pal_even.hex")) u_ram0(
    .clk0   ( clk24        ),
    .clk1   ( clk          ),
    // Port 0
    .data0  ( cpu_dout     ),
    .addr0  ( pal_addr     ),
    .we0    ( pal0_we      ),
    .q0     ( pal_even     ),
    // Port 1
    .data1  (              ),
    .addr1  ( col_addr     ),
    .we1    ( 1'b0         ),
    .q1     ( co_bus[7:0]  )
);

jtframe_dual_ram #(.aw(8),.simhexfile("pal_odd.hex")) u_ram1(
    .clk0   ( clk24        ),
    .clk1   ( clk          ),
    // Port 0
    .data0  ( cpu_dout     ),
    .addr0  ( pal_addr     ),
    .we0    ( pal1_we      ),
    .q0     ( pal_odd      ),
    // Port 1
    .data1  (              ),
    .addr1  ( col_addr     ),
    .we1    ( 1'b0         ),
    .q1     ( co_bus[15:8] )
);

`ifdef GRAY
always @(*) //if(pxl_cen) 
    col_in = {4{col_addr[3:0]}};
`else
always @(*) //if(pxl_cen) 
    col_in = co_bus & {16{black_n}};
`endif

jtframe_blank #(.DLY(2),.DW(16)) u_blank(
    .clk        ( clk       ),
    .pxl_cen    ( pxl_cen   ),
    .LHBL       ( LHBL      ),
    .LVBL       ( LVBL      ),
    .LHBL_dly   ( LHBL_dly  ),
    .LVBL_dly   ( LVBL_dly  ),
    .preLBL     (           ),
    .rgb_in     ( col_in    ),
    .rgb_out    ( col_out   )
);


endmodule