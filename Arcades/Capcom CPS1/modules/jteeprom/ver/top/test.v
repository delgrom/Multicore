/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns/1ps

module test;

reg clk, rst, sclk, di, cs;
integer cnt, start=80;
wire    do;

localparam CMDCNT=3;

reg [2:0] cmd[0:64*CMDCNT-1];

initial begin
    $readmemb("read_110010.bin", cmd,0,63);
    $readmemb("write_110010.bin", cmd,64,64*2-1);
    $readmemb("read_110010.bin", cmd,64*2,64*3-1);
end

initial begin
    clk  = 0;
    di   = 0;
    cs   = 0;
    sclk = 0;
    cnt  = 0;
    forever #20 clk = ~clk;
end

initial begin
    rst = 0;
    #50 rst = 1;
    #50 rst = 0;
end

always @(posedge clk) begin
    if( start ) start = start-1;
    else begin
        { cs, sclk, di } <= cmd[cnt];
        cnt <= cnt+1;
        if( cnt == 64*CMDCNT-1 ) $finish;
    end
end

reg [15:0] read_data;

always @(negedge sclk)
    read_data <= { read_data[14:0], do };

jt9346 UUT(
    .clk    ( clk   ),
    .rst    ( rst   ),
    .sclk   ( sclk  ),
    .di     ( di    ),
    .do     ( do    ),
    .cs     ( cs    )
);

initial begin
    $dumpfile("test.lxt");
    $dumpvars;
end

endmodule