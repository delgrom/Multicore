/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//********************************************
   RAM Modules for "FPGA Gaplus"

            Copyright (c) 2007,2019 MiSTer-X
*********************************************/

module DPRAM_1024V( CL0, ADRS0, IN0, OUT0, WR0, CL1, ADRS1, OUT1 );
input           CL0;
input       [9:0]   ADRS0;
input       [7:0]   IN0;
output  [7:0]   OUT0;
input               WR0;
input           CL1;
input       [9:0]   ADRS1;
output  [7:0]   OUT1;

reg [7:0] ramcore[0:1023];
reg [7:0] OUT0;
reg [7:0] OUT1;

always @( posedge CL0 ) begin
    if ( WR0 ) ramcore[ADRS0] <= IN0;
    else OUT0 <= ramcore[ADRS0];
end

always @( posedge CL1 ) begin
    OUT1 <= ramcore[ADRS1];
end

endmodule


module DPRAM_2048( CL0, ADRS0, IN0, OUT0, WR0, CL1, ADRS1, IN1, OUT1, WR1 );

    input          CL0;
    input  [10:0]  ADRS0;  
    input  [7:0]   IN0;
    output [7:0]   OUT0;
    input          WR0;

    input          CL1;
    input  [10:0]  ADRS1;  
    input  [7:0]   IN1;
    output [7:0]   OUT1;
    input          WR1;

    reg [7:0] ramcore[0:2047];
    reg [7:0] OUT0;
    reg [7:0] OUT1;

    always @( posedge CL0 ) begin
        if ( WR0 ) ramcore[ADRS0] <= IN0;
        else OUT0 <= ramcore[ADRS0];
    end

    always @( posedge CL1 ) begin
        if ( WR1 ) ramcore[ADRS1] <= IN1;
        else OUT1 <= ramcore[ADRS1];
    end

endmodule


module DPRAM_2048V( CL0, ADRS0, IN0, OUT0, WR0, CL1, ADRS1, OUT1 );

    input          CL0;
    input  [10:0]  ADRS0;  
    input  [7:0]   IN0;
    output [7:0]   OUT0;
    input          WR0;

    input          CL1;
    input  [10:0]  ADRS1;  
    output [7:0]   OUT1;

    reg [7:0] ramcore[0:2047];
    reg [7:0] OUT0;
    reg [7:0] OUT1;

    always @( posedge CL0 ) begin
        if ( WR0 ) ramcore[ADRS0] <= IN0;
        else OUT0 <= ramcore[ADRS0];
    end

    always @( posedge CL1 ) begin
        OUT1 <= ramcore[ADRS1];
    end

endmodule

