--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- Audio for Super Breakout
-- This is a very simple circuit, tones are created by gating signals from the vertical scan counter
-- Original hardware used resistors to mix the tones, here we are mixing them digitally and using a delta-sigma
-- DAC to produce audio on a single pin
-- (c) 2017 James Sweet
--
-- This is free software: you can redistribute
-- it and/or modify it under the terms of the GNU General
-- Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your
-- option) any later version.
--
-- This is distributed in the hope that it will
-- be useful, but WITHOUT ANY WARRANTY; without even the
-- implied warranty of MERCHANTABILITY or FITNESS FOR A
-- PARTICULAR PURPOSE. See the GNU General Public License
-- for more details.

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity audio is 
port(		
			Reset_n		: in	std_logic;
			Tones_n		: in	std_logic;
			Display		: in	std_logic_vector(3 downto 0);
			VCount		: in  std_logic_vector(7 downto 0);
			Audio_PWM	: out std_logic_vector(7 downto 0));
end audio;

architecture rtl of audio is

signal reset		: std_logic;
signal V32				: std_logic;
signal V16				: std_logic;
signal V8				: std_logic;
signal V4				: std_logic;
signal tone_V4		: std_logic;
signal tone_V8 	: std_logic;
signal tone_V16	: std_logic;
signal tone_V32	: std_logic;
signal tone_reg 	: std_logic_vector(3 downto 0);

begin

reset <= (not reset_n);

V32 <= Vcount(5);
V16 <= Vcount(4);
V8 <= Vcount(3);
V4 <= Vcount(2);

C4: process(tones_n, V4, V8, V16, V32, display) is
begin
	if tones_n <= '0' then
		tone_reg <= display;
	end if;
end process;

tone_V4 <= tone_reg(0) and V4;
tone_V8 <= tone_reg(1) and V8;
tone_V16 <= tone_reg(2) and V16;
tone_V32 <= tone_reg(3) and V32;

Audio_PWM <= tone_V4 & '0' & tone_V8 & '0' & tone_V16 & '0' & tone_V32 & '0';


end rtl;