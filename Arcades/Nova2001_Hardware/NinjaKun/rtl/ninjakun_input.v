/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// Copyright (c) 2011 MiSTer-X

module ninjakun_input
(
	input				MCLK,
	input				RESET,

	input [7:0]		CTR1i,	// Control Panel (Negative Logic)
	input	[7:0]		CTR2i,

	input				VBLK, 

	input [1:0]		AD0,
	input	[1:0]		OD0,
	input				WR0,

	input [1:0]		AD1,
	input	[1:0]		OD1,
	input				WR1,

	output [7:0]	INPD0,
	output [7:0]	INPD1
);

reg [1:0] SYNCFLG;
reg [7:0] CTR1,CTR2;
always @( posedge MCLK or posedge RESET ) begin
	if (RESET) begin
		SYNCFLG = 0;
	end
	else begin
		CTR1 <= CTR1i;
		CTR2 <= CTR2i;
		if (WR0) begin
			if (OD0[1]) SYNCFLG[0] = 1;
			if (OD0[0]) SYNCFLG[1] = 0;
		end
		if (WR1) begin
			if (OD1[1]) SYNCFLG[0] = 0;
			if (OD1[0]) SYNCFLG[1] = 1;
		end
	end
end

wire [7:0] INPORT0 = CTR1;
wire [7:0] INPORT1 = CTR2;
wire [7:0] INPORT2 = { 4'b0000, SYNCFLG, ~VBLK,1'b0 };

assign INPD0 = ( AD0 == 0 ) ? INPORT0 :
					( AD0 == 1 ) ? INPORT1 :
					( AD0 == 2 ) ? INPORT2 : 8'hFF;

assign INPD1 = ( AD1 == 0 ) ? INPORT0 :
					( AD1 == 1 ) ? INPORT1 :
					( AD1 == 2 ) ? INPORT2 : 8'hFF;

endmodule 