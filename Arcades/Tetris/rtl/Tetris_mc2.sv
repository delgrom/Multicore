/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//
//  Multicore 2 Top by Victor Trucco
//
//============================================================================

`default_nettype none

module Tetris_mc2(
    // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAMs (AS7C34096)
    output wire [18:0]sram_addr_o  = 18'b0000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM    (H57V256)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io          = 1'bz,
    inout wire  ps2_data_io         = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o           = 1'b1,
    output wire sd_sclk_o           = 1'b0,
    output wire sd_mosi_o           = 1'b0,
    input wire  sd_miso_i,

    // Joysticks
    input wire  joy1_up_i,
    input wire  joy1_down_i,
    input wire  joy1_left_i,
    input wire  joy1_right_i,
    input wire  joy1_p6_i,
    input wire  joy1_p9_i,
    input wire  joy2_up_i,
    input wire  joy2_down_i,
    input wire  joy2_left_i,
    input wire  joy2_right_i,
    input wire  joy2_p6_i,
    input wire  joy2_p9_i,
    output wire joyX_p7_o           = 1'b1,

    // Audio
    output        AUDIO_L,
    output        AUDIO_R,
    input wire  ear_i,
    output wire mic_o                   = 1'b0,

        // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

        // HDMI
    output wire [7:0]tmds_o         = 8'b00000000,

        //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
        
    inout wire  stm_b8_io, 
    inout wire  stm_b9_io,

    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2
);

`include "rtl/build_id.v" 

localparam CONF_STR = {
    "P,Tetris.dat;", //13
    "O2,Service,Off,On;", //18,
//  "O34,Scanlines,Off,25%,50%,75%;", //30
    "O35,Scandoubler Fx,None,HQ2x,CRT 25%,CRT 50%,CRT 75%;", //53
    "O6,Blend,Off,On;", //16
    "T0,Reset;", //9
    "V,v1.0." //7
};

localparam STRLEN = 13 + 18 + 53 + 16 + 9 + 7; 

//assign LED = ~ioctl_downl;
assign  stm_rst_o           = 1'bz;
//assign SDRAM_CLK = clk_sd;
assign SDRAM_CKE = 1;
assign AUDIO_R = AUDIO_L;

wire clk_sys, clk_sd;
wire clk_hdmi, clk_hdmix5;
wire pll_locked;
pll_mist pll(
    .inclk0(clock_50_i),
    .areset(0),
    .c0(clk_sd),//3xclk_sys
    .c1(clk_sys),//14.318,
    .c2(SDRAM_CLK),
    .c3(clk_hdmi), //57
    .c4(clk_hdmix5), //143
    .locked(pll_locked)
    );

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire [15:0] audio;
wire        hs, vs, hb, vb;
wire        blankn = ~(hb | vb);
wire  [2:0] g, r;
wire  [1:0] b;
wire [15:0] rom_addr;
wire [15:0] rom_do;
wire [15:0] gfx_addr;
wire [15:0] gfx_do;
wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

data_io #(.STRLEN (STRLEN)) data_io(
    .clk_sys       ( clk_sd      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in            ( osd_s & keys_s ),
    .conf_str       ( CONF_STR ),
    .status         ( status ),
    
    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);
        
reg port1_req, port2_req;
sdram sdram(
    .*,
    .init_n        ( pll_locked   ),
    .clk           ( clk_sd       ),
    .clkref        ( PCLK         ),

    // port1 used for main CPU
    .port1_req     ( port1_req    ),
    .port1_ack     ( ),
    .port1_a       ( ioctl_addr[23:1] ),
    .port1_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port1_we      ( ioctl_downl ),
    .port1_d       ( {ioctl_dout, ioctl_dout} ),
    .port1_q       ( ),

    .cpu1_addr     ( ioctl_downl ? 16'hffff : {2'b0, rom_addr[15:1]}),
    .cpu1_q        ( rom_do ),

    // port2 for gfx
    .port2_req     ( port2_req ),
    .port2_ack     ( ),
    .port2_a       ( ioctl_addr[23:1] - 16'h8000 ),
    .port2_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port2_we      ( ioctl_downl ),
    .port2_d       ( {ioctl_dout, ioctl_dout} ),
    .port2_q       ( ),

    .gfx_addr      ( gfx_addr[15:1] ),
    .gfx_q         ( gfx_do )
);

always @(posedge clk_sd) begin
    reg        ioctl_wr_last = 0;

    ioctl_wr_last <= ioctl_wr;
    if (ioctl_downl) begin
        if (~ioctl_wr_last && ioctl_wr) begin
            port1_req <= ~port1_req;
            port2_req <= ~port2_req;
        end
    end
end

reg reset = 1;
reg rom_loaded = 0;
always @(posedge clk_sd) begin
    reg ioctl_downlD;
    ioctl_downlD <= ioctl_downl;

    if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
    reset <= status[0] | ~btn_n_i[4] | ~rom_loaded;
end

wire [10:0] INP = ~{status[2],1'b1, (JoyPCFRLDU[5] | ~btn_n_i[3]), m_left2, m_right2, m_down2, m_fire2, m_left1, m_right1, m_down1, m_fire1};

FPGA_ATetris FPGA_ATetris(
    .MCLK(clk_sys),     // 14.318MHz
    .RESET(reset),
    
    .INP(INP),      // Negative Logic

    .HPOS(HPOS),
    .VPOS(VPOS),
    .PCLK(PCLK),
    .PCLK_EN(PCLK_EN),
    .POUT(POUT),
    
    .AOUT(audio),
    
    .PRAD(rom_addr),
    .PRDT(rom_addr[0] ? rom_do[15:8] : rom_do[7:0]),

    .CRAD(gfx_addr),
    .CRDT(gfx_do)
);

wire            PCLK;
wire            PCLK_EN;
wire  [8:0] HPOS,VPOS;
wire  [7:0] POUT;
hvgen hvgen(
    .MCLK(clk_sys),
    .PCLK_EN(PCLK_EN),
    .HPOS(HPOS),
    .VPOS(VPOS),
    .iRGB(POUT),
    .oRGB({r,g,b}),
    .HBLK(hb),
    .VBLK(vb),
    .HSYN(hs),
    .VSYN(vs)
    );

wire [5:0] vga_r_s; 
wire [5:0] vga_g_s; 
wire [5:0] vga_b_s; 
    
//mist_video #(.COLOR_DEPTH(3), .SD_HCNT_WIDTH(10)) mist_video(
//  .clk_sys        ( clk_sys          ),
//  .SPI_SCK        ( SPI_SCK          ),
//  .SPI_SS3        ( SPI_SS2          ),
//  .SPI_DI         ( SPI_DI           ),
//  .R              ( blankn ? r : 0   ),
//  .G              ( blankn ? g : 0   ),
//  .B              ( blankn ? {b,b[0]} : 0 ),
//  .HSync          ( hs               ),
//  .VSync          ( vs               ),
//  .VGA_R          ( vga_r_s          ),
//  .VGA_G          ( vga_g_s          ),
//  .VGA_B          ( vga_b_s          ),
//  .VGA_VS         ( VGA_VS           ),
//  .VGA_HS         ( VGA_HS           ),
//  .ce_divider     ( 1'b1             ),
//  .blend          ( status[5]        ),
//  .scandoubler_disable(scandoublerD  ),
//  .scanlines      ( status[4:3]      ),
//  .ypbpr          ( ypbpr            )
//  );

//assign VGA_R = vga_r_s[5:1];
//assign VGA_G = vga_g_s[5:1];
//assign VGA_B = vga_b_s[5:1];

dac #(
    .C_bits(16))
dac_l(
    .clk_i(clk_sys),
    .res_n_i(1),
    .dac_i(audio),
    .dac_o(AUDIO_L)
    );

wire m_down1   = JoyPCFRLDU[1] | ~joy1_s[1];
wire m_left1   = JoyPCFRLDU[2] | ~joy1_s[2];
wire m_right1  = JoyPCFRLDU[3] | ~joy1_s[3];
wire m_fire1   = JoyPCFRLDU[4] | ~joy1_s[4] | ~joy1_s[5] | ~joy1_s[6];

wire m_down2   = ~joy2_s[1];
wire m_left2   = ~joy2_s[2];
wire m_right2  = ~joy2_s[3];
wire m_fire2   = ~joy2_s[4] | ~joy2_s[5] | ~joy2_s[6];



wire       key_pressed;
wire [7:0] key_code;
wire       key_strobe;



wire kbd_intr;
wire [7:0] JoyPCFRLDU;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_sys ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

//translate scancode to joystick
kbd_joystick k_joystick
(
  .clk          ( clk_sys ),
  .kbdint       ( kbd_intr ),
  .kbdscancode  ( kbd_scancode ), 
  .JoyPCFRLDU     ( JoyPCFRLDU ),
  .osd_o              ( keys_s )
);


//--- Joystick read with sega 6 button support----------------------
    


    reg [11:0]joy1_s;   
    reg [11:0]joy2_s; 
    reg joyP7_s;

    reg [7:0]state_v = 8'd0;
    reg j1_sixbutton_v = 1'b0;
    reg j2_sixbutton_v = 1'b0;
    
    always @(negedge hs) 
    begin
        

            state_v <= state_v + 1;

            
            case (state_v)          //-- joy_s format MXYZ SACB RLDU
                8'd0:  
                    joyP7_s <=  1'b0;
                    
                8'd1:
                    joyP7_s <=  1'b1;

                8'd2:
                    begin
                        joy1_s[3:0] <= {joy1_right_i, joy1_left_i, joy1_down_i, joy1_up_i}; //-- R, L, D, U
                        joy2_s[3:0] <= {joy2_right_i, joy2_left_i, joy2_down_i, joy2_up_i}; //-- R, L, D, U
                        joy1_s[5:4] <= {joy1_p9_i, joy1_p6_i}; //-- C, B
                        joy2_s[5:4] <= {joy2_p9_i, joy2_p6_i}; //-- C, B                    
                        joyP7_s <= 1'b0;
                        j1_sixbutton_v <= 1'b0; //-- Assume it's not a six-button controller
                        j2_sixbutton_v <= 1'b0; //-- Assume it's not a six-button controller
                    end
                    
                8'd3:
                    begin
                        if (joy1_right_i == 1'b0 && joy1_left_i == 1'b0) // it's a megadrive controller
                                joy1_s[7:6] <= { joy1_p9_i , joy1_p6_i }; //-- Start, A
                        else
                                joy1_s[7:4] <= { 1'b1, 1'b1, joy1_p9_i, joy1_p6_i }; //-- read A/B as master System
                            
                        if (joy2_right_i == 1'b0 && joy2_left_i == 1'b0) // it's a megadrive controller
                                joy2_s[7:6] <= { joy2_p9_i , joy2_p6_i }; //-- Start, A
                        else
                                joy2_s[7:4] <= { 1'b1, 1'b1, joy2_p9_i, joy2_p6_i }; //-- read A/B as master System

                            
                        joyP7_s <= 1'b1;
                    end
                    
                8'd4:  
                    joyP7_s <= 1'b0;

                8'd5:
                    begin
                        if (joy1_right_i == 1'b0 && joy1_left_i == 1'b0 && joy1_down_i == 1'b0 && joy1_up_i == 1'b0 )
                            j1_sixbutton_v <= 1'b1; // --it's a six button
                        
                        
                        if (joy2_right_i == 1'b0 && joy2_left_i == 1'b0 && joy2_down_i == 1'b0 && joy2_up_i == 1'b0 )
                            j2_sixbutton_v <= 1'b1; // --it's a six button
                        
                        
                        joyP7_s <= 1'b1;
                    end
                    
                8'd6:
                    begin
                        if (j1_sixbutton_v == 1'b1)
                            joy1_s[11:8] <= { joy1_right_i, joy1_left_i, joy1_down_i, joy1_up_i }; //-- Mode, X, Y e Z
                        
                        
                        if (j2_sixbutton_v == 1'b1)
                            joy2_s[11:8] <= { joy2_right_i, joy2_left_i, joy2_down_i, joy2_up_i }; //-- Mode, X, Y e Z
                        
                        
                        joyP7_s <= 1'b0;
                    end 
                    
                default:
                    joyP7_s <= 1'b1;
                    
            endcase

    end
    
    assign joyX_p7_o = joyP7_s;
    //---------------------------
    wire hard_reset = ~pll_locked;

        reg [15:0] power_on_s   = 16'b1111111111111111;
        reg [7:0] osd_s = 8'b11111111;
        
        //--start the microcontroller OSD menu after the power on
        always @(posedge clk_sys) 
        begin
        
                if (hard_reset == 1)
                    power_on_s = 16'b1111111111111111;
                else if (power_on_s != 0)
                begin
                    power_on_s = power_on_s - 1;

                    osd_s = 8'b00111111;
                end 

                    
                
                if (ioctl_downl == 1 && osd_s == 8'b00111111)
                    osd_s = 8'b11111111;
            
        end 
        
        //------ HDMI ---------------------------------
        
        

        
        
        reg ce_pix, ce_vid;
        reg clk_hdmi_px = 0;
        
        always @(posedge clk_hdmi) begin
            reg old_clk;
            old_clk <= ce_vid;
            ce_pix  <= old_clk & ~ce_vid;
            clk_hdmi_px <= ~clk_hdmi_px;
        end

        assign ce_vid = PCLK;
        
            

    
        arcade_fx #(336,8) arcade_video
        (

            .clk_video(clk_hdmi),
            .ce_pix(ce_pix),

            .RGB_in({r,g,b}),
            .HBlank(hb),
            .VBlank(vb),
            .HSync(~hs),
            .VSync(~vs),
            
            .SPI_DI  ( SPI_DI  ),
            .SPI_SCK ( SPI_SCK ),
            .SPI_SS3 ( SPI_SS2 ),
            
            .HDMI_R     (HDMI_R),
            .HDMI_G     (HDMI_G),
            .HDMI_B     (HDMI_B),
            .HDMI_HS (HDMI_HS),
            .HDMI_VS (HDMI_VS),
            .HDMI_DE (HDMI_DE),
        
            .fx(status[5:3]),
            .blend (status[6]),
            
            .forced_scandoubler(1'b1)

        );
        

    assign VGA_R    = HDMI_R[7:4];
    assign VGA_G    = HDMI_G[7:4];
    assign VGA_B    = HDMI_B[7:4];
    assign VGA_HS   = HDMI_HS;
    assign VGA_VS   = HDMI_VS;
            
            
            
        
    wire [7:0] HDMI_R;
    wire [7:0] HDMI_G;
    wire [7:0] HDMI_B;
    wire HDMI_HS;
    wire HDMI_VS;
    wire HDMI_DE;
    
    wire [9:0] tdms_r_s;
    wire [9:0] tdms_g_s;
    wire [9:0] tdms_b_s;

    wire [3:0] hdmi_p_s;
    wire [3:0] hdmi_n_s;

    hdmi
    #
    (
        .FREQ   ( 28610000 ),   // -- pixel clock frequency = 25.2MHz
        .FS ( 48000 ),      // -- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
        .CTS    ( 28610 ),      // -- CTS = Freq(pixclk) * N / (128 * Fs)
        .N      ( 6144 )            // -- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
    )
    inst_hdmi
    (
        .I_CLK_PIXEL     ( clk_hdmi_px ),

        .I_R                 ( HDMI_R ), 
        .I_G                 ( HDMI_G ),
        .I_B                 ( HDMI_B ),
         
        .I_BLANK             ( ~HDMI_DE ),
        .I_HSYNC             ( HDMI_HS ),
        .I_VSYNC             ( HDMI_VS ),
         
        .I_AUDIO_ENABLE ( 1 ),
        .I_AUDIO_PCM_L  ({ 1'b0, audio[15:1] }),
        .I_AUDIO_PCM_R   ({ 1'b0, audio[15:1] }),
 
        .O_RED           ( tdms_r_s ),
        .O_GREEN             ( tdms_g_s ),
        .O_BLUE          ( tdms_b_s )
    );

        hdmi_out_altera hdmi_io
        (
            .clock_pixel_i      ( clk_hdmi_px ),
            .clock_tdms_i       ( clk_hdmix5 ),
            .red_i              ( tdms_r_s ),
            .green_i                ( tdms_g_s ),
            .blue_i             ( tdms_b_s ),
            .tmds_out_p         ( hdmi_p_s ),
            .tmds_out_n         ( hdmi_n_s )
        );
        
        
        assign tmds_o[7] = hdmi_p_s[2]; //-- 2+     
        assign tmds_o[6] = hdmi_n_s[2]; //-- 2-     
        assign tmds_o[5] = hdmi_p_s[1]; //-- 1+         
        assign tmds_o[4] = hdmi_n_s[1]; //-- 1-     
        assign tmds_o[3] = hdmi_p_s[0]; //-- 0+     
        assign tmds_o[2] = hdmi_n_s[0]; //-- 0- 
        assign tmds_o[1] = hdmi_p_s[3]; //-- CLK+   
        assign tmds_o[0] = hdmi_n_s[3]; //-- CLK-   
    

    



endmodule 
