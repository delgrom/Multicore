--//============================================================================
--//
--//  Multicore 2+ Top by Victor Trucco
--//
--//============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

library work;
use work.pace_pkg.all;
use work.video_controller_pkg.all;

entity mpatrol is
  port
    (
      
        -- Clocks
        clock_50_i         : in    std_logic;

        -- Buttons
        btn_n_i            : in    std_logic_vector(4 downto 1);

        -- SRAM
        sram_addr_o        : out   std_logic_vector(20 downto 0)   := (others => '0');
        sram_data_io       : inout std_logic_vector(7 downto 0)    := (others => 'Z');
        sram_we_n_o        : out   std_logic                               := '1';
        sram_oe_n_o        : out   std_logic                               := '1';

        -- SDRAM
        SDRAM_A            : out std_logic_vector(12 downto 0);
        SDRAM_DQ           : inout std_logic_vector(15 downto 0);

        SDRAM_BA           : out std_logic_vector(1 downto 0);
        SDRAM_DQMH         : out std_logic;
        SDRAM_DQML         : out std_logic;    

        SDRAM_nRAS         : out std_logic;
        SDRAM_nCAS         : out std_logic;
        SDRAM_CKE          : out std_logic;
        SDRAM_CLK          : out std_logic;
        SDRAM_nCS          : out std_logic;
        SDRAM_nWE          : out std_logic;
    
        -- PS2
        ps2_clk_io         : inout std_logic                        := 'Z';
        ps2_data_io        : inout std_logic                        := 'Z';
        ps2_mouse_clk_io   : inout std_logic                        := 'Z';
        ps2_mouse_data_io  : inout std_logic                        := 'Z';

        -- SD Card
        sd_cs_n_o          : out   std_logic                        := 'Z';
        sd_sclk_o          : out   std_logic                        := 'Z';
        sd_mosi_o          : out   std_logic                        := 'Z';
        sd_miso_i          : in    std_logic;

        -- Joysticks
        joy_clock_o        : out   std_logic;
        joy_load_o         : out   std_logic;
        joy_data_i         : in    std_logic;
        joy_p7_o           : out   std_logic                        := '1';

        -- Audio
        AUDIO_L             : out   std_logic                       := '0';
        AUDIO_R             : out   std_logic                       := '0';
        ear_i               : in    std_logic;
        mic_o               : out   std_logic                       := '0';

        -- VGA
        VGA_R               : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_G               : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_B               : out   std_logic_vector(4 downto 0)    := (others => '0');
        VGA_HS              : out   std_logic                       := '1';
        VGA_VS              : out   std_logic                       := '1';

        LED                 : out   std_logic                       := '1';-- 0 is led on

        --STM32
        stm_rx_o            : out std_logic     := 'Z'; -- stm RX pin, so, is OUT on the slave
        stm_tx_i            : in  std_logic     := 'Z'; -- stm TX pin, so, is IN on the slave
        stm_rst_o           : out std_logic     := 'Z'; -- '0' to hold the microcontroller reset line, to free the SD card
        
        SPI_SCK             : in  std_logic;
        SPI_DO              : out std_logic   := 'Z';
        SPI_DI              : in  std_logic;
        SPI_SS2             : in  std_logic;
        SPI_nWAIT           : out std_logic   := '1';

        GPIO                : inout std_logic_vector(31 downto 0)   := (others => 'Z')
  );    
  
end mpatrol;

architecture SYN of mpatrol is


    type config_array is array(natural range 15 downto 0) of std_logic_vector(7 downto 0);
    signal config_buffer_s : config_array;

    component joystick_serial is
    port
    (
        clk_i           : in  std_logic;
        joy_data_i      : in  std_logic;
        joy_clk_o       : out  std_logic;
        joy_load_o      : out  std_logic;

        joy1_up_o       : out std_logic;
        joy1_down_o     : out std_logic;
        joy1_left_o     : out std_logic;
        joy1_right_o    : out std_logic;
        joy1_fire1_o    : out std_logic;
        joy1_fire2_o    : out std_logic;
        joy2_up_o       : out std_logic;
        joy2_down_o     : out std_logic;
        joy2_left_o     : out std_logic;
        joy2_right_o    : out std_logic;
        joy2_fire1_o    : out std_logic;
        joy2_fire2_o    : out std_logic
    );
    end component;


    component data_io
    generic
    (
       STRLEN : integer :=   0
    );
    port
    (
        clk_sys                   : in std_logic;
        SPI_SCK, SPI_SS2, SPI_DI :in std_logic;
        SPI_DO : out std_logic;

        data_in       : in std_logic_vector(7 downto 0);
        conf_str       : in std_logic_vector((8*STRLEN)-1 downto 0);
        status        : out std_logic_vector(31 downto 0);

        config_buffer_o : out config_array;



      --  clkref_n          : in  std_logic := '0';
        ioctl_download    : out std_logic;
        ioctl_index       : out std_logic_vector(7 downto 0);
        ioctl_wr          : out std_logic;
        ioctl_addr        : out std_logic_vector(24 downto 0);
        ioctl_dout        : out std_logic_vector(7 downto 0)
    );
  end component;

  component mist_video
generic (
    OSD_COLOR    : std_logic_vector(2 downto 0) := "110";
    OSD_X_OFFSET : std_logic_vector(9 downto 0) := (others => '0');
    OSD_Y_OFFSET : std_logic_vector(9 downto 0) := (others => '0');
    SD_HCNT_WIDTH: integer := 9;
    COLOR_DEPTH  : integer := 6;
    OSD_AUTO_CE  : boolean := true;
    USE_FRAMEBUFFER :integer := 0
);
port (
    clk_sys     : in std_logic;

    SPI_SCK     : in std_logic;
    SPI_SS3     : in std_logic;
    SPI_DI      : in std_logic;

    scanlines   : in std_logic_vector(1 downto 0);
    ce_divider  : in std_logic := '0';
    scandoubler_disable : in std_logic;

    rotate      : in std_logic_vector(1 downto 0);
  
    blend       : in std_logic := '0';
    no_csync    : in std_logic := '0';

    HSync       : in std_logic;
    VSync       : in std_logic;
    R           : in std_logic_vector(COLOR_DEPTH-1 downto 0);
    G           : in std_logic_vector(COLOR_DEPTH-1 downto 0);
    B           : in std_logic_vector(COLOR_DEPTH-1 downto 0);

    VGA_HS      : out std_logic;
    VGA_VS      : out std_logic;
    VGA_R       : out std_logic_vector(4 downto 0);
    VGA_G       : out std_logic_vector(4 downto 0);
    VGA_B       : out std_logic_vector(4 downto 0);
    osd_enable      : out std_logic
);
end component mist_video;

    component PumpSignal
    port(
        clk_i       : in  std_logic;
        reset_i     : in  std_logic;
        download_i  : in  std_logic;   
        pump_o      : out std_logic_vector( 7 downto 0)
    );
    end component;

  signal init               : std_logic := '1';  
  signal clk_sys        : std_logic;
  signal clk_aud        : std_logic;
  signal clk_vid        : std_logic;
  signal rst_audD       : std_logic;
  signal rst_aud        : std_logic;
  signal clkrst_i       : from_CLKRST_t;
  signal buttons_i      : from_BUTTONS_t;
  signal switches_i     : from_SWITCHES_t;
  signal leds_o         : to_LEDS_t;
  signal inputs_i       : from_INPUTS_t;
  signal video_i        : from_VIDEO_t;
  signal video_o        : to_VIDEO_t;
  --MIST
  signal audio          : std_logic;
  signal status             : std_logic_vector(63 downto 0);
  signal joystick1      : std_logic_vector(31 downto 0);
  signal joystick2      : std_logic_vector(31 downto 0);
  signal joystick       : std_logic_vector(7 downto 0);
  signal kbd_joy            : std_logic_vector(9 downto 0);  
  signal switches       : std_logic_vector(1 downto 0);
  signal buttons        : std_logic_vector(1 downto 0);
  signal ps2_kbd_clk    : std_logic;
  signal ps2_kbd_data   : std_logic;
  signal scandoubler_disable   : std_logic;
  signal ypbpr          : std_logic;
  signal no_csync       : std_logic;
  signal reset          : std_logic;  
  signal audio_out      : std_logic_vector(11 downto 0);
  signal sound_data     : std_logic_vector(7 downto 0);
  
  constant CONF_STR : string :=
    "P,Moon Patrol.ini;"&
    "O12,Scanlines,Off,25%,50%,75%;"&
    "OD,Blend,Off,On;"&
    "OC,Scandoubler,On,Off;"&
    --"OB,Video timings,Original,PAL;"&
    "O34,Patrol cars,5,3,2,1;"&
    "O56,New car at,10/30/50K,20/40/60K,10K,Never;"&
    "OA,Freeze,Disable,Enable;"&
    "O7,Demo mode,Off,On;"&
    "O8,Sector selection,Off,On;"&
    "O9,Test mode,Off,On;"&
    "T0,Reset;";

  -- convert string to std_logic_vector to be given to user_io
  function to_slv(s: string) return std_logic_vector is
    constant ss: string(1 to s'length) := s;
    variable rval: std_logic_vector(1 to 8 * s'length);
    variable p: integer;
    variable c: integer;
  begin
    for i in ss'range loop
      p := 8 * i;
      c := character'pos(ss(i));
      rval(p - 7 to p) := std_logic_vector(to_unsigned(c,8));
    end loop;
    return rval;
  end function;

component keyboard
   port  (
        clk                     :in STD_LOGIC;
        reset                       :in STD_LOGIC;
        ps2_kbd_clk             :in STD_LOGIC;
        ps2_kbd_data            :in STD_LOGIC;
        joystick                    :out STD_LOGIC_VECTOR(9 downto 0));
end component;
  
signal pll_locked : std_logic;
signal downl : std_logic;
signal osd_s : std_logic_vector(7 downto 0);

signal joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i : std_logic;
signal joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i : std_logic;

signal keys_s       : std_logic_vector(7 downto 0) := (others => '1'); 
signal key_code     : std_logic_vector(7 downto 0) := (others => '0'); 
signal key_strobe   : std_logic;
signal direct_video   : std_logic;
signal direct_video_s   : std_logic;
signal osd_enable   : std_logic;

signal clock_div_q : std_logic;

signal color_s : std_logic_vector(8 downto 0);
signal vga_col_s : std_logic_vector(8 downto 0);
signal vga_hs_s : std_logic;
signal vga_vs_s : std_logic;

signal clk_25m2 : std_logic;
signal clk_40 : std_logic;

begin

  SDRAM_nCS <= '1'; -- disable ram
  sram_we_n_o <= '1';
  sram_oe_n_o <= '1';
  stm_rst_o <= 'Z';

  PumpSignal_1 : PumpSignal port map(clk_vid, (not pll_locked), downl, osd_s);

 joystick_serial1 : joystick_serial 
    port map
    (
        clk_i           => clock_div_q,
        joy_data_i      => joy_data_i,
        joy_clk_o       => joy_clock_o,
        joy_load_o      => joy_load_o,

        joy1_up_o       => joy1_up_i,
        joy1_down_o     => joy1_down_i,
        joy1_left_o     => joy1_left_i,
        joy1_right_o    => joy1_right_i,
        joy1_fire1_o    => joy1_p6_i,
        joy1_fire2_o    => joy1_p9_i,

        joy2_up_o       => joy2_up_i,
        joy2_down_o     => joy2_down_i,
        joy2_left_o     => joy2_left_i,
        joy2_right_o    => joy2_right_i,
        joy2_fire1_o    => joy2_p6_i,
        joy2_fire2_o    => joy2_p9_i
    );


--CLOCK
Clock_inst : entity work.Clock
    port map (
        inclk0  => clock_50_i,
        c0      => clk_aud,    -- 3.58/4
        c1      => clk_sys,    -- 6
        c2      => clk_vid,    -- 24
        locked  => pll_locked
    );


pll_vga : work.pll_vga
port map
(
        inclk0  => clock_50_i,
        c0      => clk_25m2,    
        c1      => clk_40
);

    clkrst_i.clk(0) <=  clk_sys;
    clkrst_i.clk(1) <=  clk_sys;

    video_i.clk     <= clk_sys;
    video_i.clk_ena <= '1';
    video_i.reset   <= clkrst_i.rst(1);

--RESET
    process (clk_sys)
        variable count : std_logic_vector (11 downto 0) := (others => '0');
    begin
        if rising_edge(clk_sys) then
            if count = X"FFF" then
                init <= '0';
            else
                count := count + 1;
                init <= '1';
            end if;
        end if;
    end process;

    process (clk_sys) begin
        if rising_edge(clk_sys) then
            clkrst_i.arst    <= init or status(0) or not btn_n_i(4);
            clkrst_i.arst_n  <= not clkrst_i.arst;
        end if;
    end process;

    process (clk_aud) begin
        if rising_edge(clk_aud) then
            rst_audD <= clkrst_i.arst;
            rst_aud  <= rst_audD;
        end if;
    end process;

  GEN_RESETS : for i in 0 to 3 generate

    process (clkrst_i)
      variable rst_r : std_logic_vector(2 downto 0) := (others => '0');
    begin
      if clkrst_i.arst = '1' then
        rst_r := (others => '1');
      elsif rising_edge(clkrst_i.clk(i)) then
        rst_r := rst_r(rst_r'left-1 downto 0) & '0';
      end if;
      clkrst_i.rst(i) <= rst_r(rst_r'left);
    end process;

  end generate GEN_RESETS;   
     
--user_io_inst : user_io
--    generic map (STRLEN => CONF_STR'length)
--    port map (
--        clk_sys             => clk_sys,
--        conf_str            => to_slv(CONF_STR),
--        SPI_CLK             => SPI_SCK,
--        SPI_SS_IO           => CONF_DATA0,
--        SPI_MOSI            => SPI_DI,
--        SPI_MISO            => SPI_DO,
--        switches            => switches,
--        buttons             => buttons,
--        scandoubler_disable => scandoubler_disable,
--        ypbpr               => ypbpr,
--        no_csync            => no_csync,
--        joystick_1          => joystick2,
--        joystick_0          => joystick1,
--        status              => status,
--        ps2_kbd_clk         => ps2_kbd_clk,
--        ps2_kbd_data        => ps2_kbd_data
--    );

--u_keyboard : keyboard
--    port  map(
--        clk             => clk_sys,
--        reset           => '0',
--        ps2_kbd_clk     => ps2_kbd_clk,
--        ps2_kbd_data    => ps2_kbd_data,
--        joystick        => kbd_joy
--);


        inputs_i.jamma_n.coin(1)        <= not ( kbd_joy(4)  or not btn_n_i(3));
        inputs_i.jamma_n.p(1).start     <= not ( kbd_joy(0)   or not btn_n_i(1) );
        inputs_i.jamma_n.p(1).up        <= not ( joystick(3) );
        inputs_i.jamma_n.p(1).down      <= not ( joystick(2) );
        inputs_i.jamma_n.p(1).left      <= not ( joystick(1) );
        inputs_i.jamma_n.p(1).right     <= not ( joystick(0) );
        inputs_i.jamma_n.p(1).button(1) <= not ( joystick(4) );
        inputs_i.jamma_n.p(1).button(2) <= not ( joystick(5) or joystick(3) );
        inputs_i.jamma_n.p(1).button(3) <= '1';
        inputs_i.jamma_n.p(1).button(4) <= '1';
        inputs_i.jamma_n.p(1).button(5) <= '1';     
        inputs_i.jamma_n.p(2).start     <= not ( kbd_joy(1)   or not btn_n_i(2) );
        inputs_i.jamma_n.p(2).up        <= not ( joystick(3) );
        inputs_i.jamma_n.p(2).down      <= not ( joystick(2) );
        inputs_i.jamma_n.p(2).left      <= not ( joystick(1) );
        inputs_i.jamma_n.p(2).right     <= not ( joystick(0) );
        inputs_i.jamma_n.p(2).button(1) <= not ( joystick(4) );
        inputs_i.jamma_n.p(2).button(2) <= not ( joystick(5) or joystick(3) ); 
        inputs_i.jamma_n.p(2).button(3) <= '1';
        inputs_i.jamma_n.p(2).button(4) <= '1';
        inputs_i.jamma_n.p(2).button(5) <= '1';
        -- not currently wired to any inputs
        inputs_i.jamma_n.coin_cnt <= (others => '1');
        inputs_i.jamma_n.coin(2) <= '1';
        inputs_i.jamma_n.service <= '1';
        inputs_i.jamma_n.tilt <= '1';
        inputs_i.jamma_n.test <= '1';

  LED <= '1';
  
moon_patrol_sound_board : entity work.moon_patrol_sound_board
    port map(
        clock_E         => clk_aud,
        areset          => rst_aud,
        select_sound    => sound_data,
        audio_out       => audio_out,
        dbg_cpu_addr    => open
    );  
  
dac : entity work.dac
  generic map (
        C_bits    => 12
    )
    port map (
        clk_i     => clk_aud,
        res_n_i   => not rst_aud,
        dac_i     => audio_out,
        dac_o     => audio
   );

AUDIO_R <= audio;
AUDIO_L <= audio;       

switches_i(15) <= not status(9); -- Test mode
switches_i(14) <= not status(7);
switches_i(13) <= not status(8); -- Sector select
switches_i(12) <= not status(10);-- Freeze enable
switches_i(11 downto 8) <= "1100";
switches_i( 7 downto 4) <= "1111";
switches_i( 1 downto 0) <= not status(4 downto 3); -- Patrol cars
switches_i( 3 downto 2) <= not status(6 downto 5); -- New car

pace_inst : entity work.pace                                            
    port map (
        clkrst_i                => clkrst_i,
        palmode         => status(11),
        buttons_i         => buttons_i,
        switches_i        => switches_i,
        leds_o            => open,
        inputs_i          => inputs_i,
        video_i           => video_i,
        video_o           => video_o,
        sound_data_o        => sound_data
    );

  data_io_inst: data_io
    generic map (STRLEN => CONF_STR'length)
    port map (
      clk_sys => clk_vid,
      SPI_SCK => SPI_SCK,
      SPI_SS2 => SPI_SS2,
      SPI_DI => SPI_DI,
      SPI_DO => SPI_DO,

      data_in => osd_s and keys_s,
      conf_str => to_slv(CONF_STR),
      status => status(31 downto 0),

      ioctl_download => downl
    );



color_s <= video_o.rgb.r(9 downto 7) & video_o.rgb.g(9 downto 7) & video_o.rgb.b(9 downto 7) when video_o.hblank = '0' and video_o.vblank = '0' else (others=>'0');

framebuffer : work.framebuffer 
generic map 
(
    WIDHT  => 252,
    HEIGHT => 240,
    DW     => 9,
    DOUBLE => 1
)
port map
(
        clk_sys    => clk_vid ,
        clk_i      => video_o.clk ,
        RGB_i      => color_s,
        hblank_i   => video_o.hblank ,
        vblank_i   => video_o.vblank ,
        dis_db_i   => status(14),
        rotate_i   => "00",

        clk_vga_i  => clk_40 , --800x600 
        RGB_o      => vga_col_s,
        hsync_o    => vga_hs_s ,
        vsync_o    => vga_vs_s ,
        blank_o    => open,

        odd_line_o => open
);

 direct_video <= (not status(12)) xor direct_video_s;

mist_video1: mist_video
    generic map (
        SD_HCNT_WIDTH => 10,
        COLOR_DEPTH => 3,
        USE_FRAMEBUFFER => 0  -- 1
    )
    port map (
        clk_sys     => clk_vid, --clk_40,
        scanlines   => status(2 downto 1),
        scandoubler_disable => direct_video,

        no_csync    => not direct_video,
        rotate      => "00",
        ce_divider  => '1',

        SPI_SCK     => SPI_SCK,
        SPI_SS3     => SPI_SS2,
        SPI_DI      => SPI_DI,

        HSync       => not video_o.hsync,
        VSync       => not video_o.vsync,
        R           => video_o.rgb.r(9 downto 7),
        G           => video_o.rgb.g(9 downto 7),
        B           => video_o.rgb.b(9 downto 7),

   --     R           => vga_col_s(8 downto 6),
   --     G           => vga_col_s(5 downto 3),
   --     B           => vga_col_s(2 downto 0),
   --     HSync       => vga_hs_s,
   --     VSync       => vga_vs_s,

        VGA_HS      => VGA_HS,
        VGA_VS      => VGA_VS,
        VGA_R       => VGA_R,
        VGA_G       => VGA_G,
        VGA_B       => VGA_B,

        osd_enable  => osd_enable,
        blend       => status(13)
    );

     --------------------------------------------------------------------------------------

    -- Keyboard clock
    process(clk_vid)
    begin
        if rising_edge(clk_vid) then 
            clock_div_q <= not clock_div_q;-- + 1;
        end if;
    end process;
    
    -- get scancode from keyboard
    keyboard_1 : entity work.io_ps2_keyboard
    generic map 
    (
        CLK_SPEED   => 12000
    )
    port map (
      clk       => clock_div_q,--(0),
      kbd_clk   => ps2_clk_io,
      kbd_dat   => ps2_data_io,
      interrupt => key_strobe,
      scancode  => key_code
    );

    -- translate scancode to joystick
    joystick_kbd : entity work.kbd_joystick
    generic map 
    (
        osd_cmd     => "011",
        USE_VKP     => '1',
        CLK_SPEED   => 12000
    )
    port map 
    (
        clk         => clock_div_q,--(0),
        kbdint      => key_strobe,
        kbdscancode => std_logic_vector(key_code), 
        osd_o       => keys_s,
        osd_enable  => osd_enable,
        direct_video => direct_video_s,
           
        -- 1,2,u,d,l,r   
        joystick_0  => joy1_p9_i & joy1_p6_i & joy1_up_i & joy1_down_i & joy1_left_i & joy1_right_i,
        joystick_1  => joy2_p9_i & joy2_p6_i & joy2_up_i & joy2_down_i & joy2_left_i & joy2_right_i,

        -- joystick_0 and joystick_1 should be swapped
        joyswap     => '0',

        -- player1 and player2 should get both joystick_0 and joystick_1
        oneplayer   => '0',

        -- tilt, coin4-1, start4-1
        controls => kbd_joy(8 downto 0),

        -- fire12-1, up, down, left, right
        player1(15 downto 8) => open,
        player1(7 downto 0)  => joystick(7 downto 0),
        player2    => open,

        -- sega joystick
        sega_strobe => joy_p7_o 
    );

end SYN;
