/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/
//============================================================================
//
//  Multicore 2+ Top by Oduvaldo Pavan Junior ( ducasp@gmail.com )
//
//============================================================================

`default_nettype none


module TheTowerofDruaga_mc2p (
// Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM    (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io          = 1'bz,
    inout wire  ps2_data_io         = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o           = 1'b1,
    output wire sd_sclk_o           = 1'b0,
    output wire sd_mosi_o           = 1'b0,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o         = 1'b1,
    output wire joy_load_o          = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o            = 1'b1,

    // Audio
    output        AUDIO_L,
    output        AUDIO_R,
    input wire  ear_i,
    output wire mic_o                   = 1'b0,

        // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

        //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card

    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output        SPI_nWAIT,
    inout [31:0]  GPIO,
    output        LED               = 1'b1 // '0' is LED on
);

`include "..\..\rtl\build_id.v" 

localparam CONF_STR = {
    "P,CORE_NAME.dat;", //16
//  "P,TowerofDruaga.dat;", //20
    "O12,Screen Rotate,0,90,180,270;",
    "OG,Scandoubler,On,Off;",
	 "O7,Double Video Buffer,On,Off;",
    "S0,ROM,Load *.ROM;", //18 
    "O34,Scanlines,Off,25%,50%,75%;", //30
    "O5,Blend,Off,On;", //16
    "T0,Reset;", //9
    "O6,Patrons list,Off,On;",
    "V,v1.00." //8
};

//assign        LED = ~ioctl_downl;
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign      AUDIO_R = AUDIO_L;
//assign        SDRAM_CLK = clock_48;
assign    SDRAM_CKE = 1;

assign      stm_rst_o = 1'bz;
assign      sram_we_n_o = 1'b1;
assign      sram_oe_n_o = 1'b1;
assign       SPI_nWAIT= 1'b1;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial joystick_serial 
(
    .clk_i           ( clk_40 ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

wire clock_48, clock_6, pll_locked;
pll pll(
    .inclk0(clock_50_i),
    .c0(clock_48),//49.147727 
    .c1(clock_6),
    .c2(SDRAM_CLK),
    .locked(pll_locked)
    );

wire clk_25m2,clk_40;
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire  [7:0] audio;
wire            hs, vs;
wire            hb, vb;
wire            blankn = ~(hb | vb);
wire [2:0]  r, g;
wire [1:0]  b;
wire [14:0] rom_addr;
wire [15:0] rom_do;
wire [12:0] snd_addr;
wire [15:0] snd_do;
wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;
wire        key_strobe;
wire        key_pressed;
wire  [7:0] key_code;

data_io #(.STRLEN(($size(CONF_STR)>>3))) data_io(
    .clk_sys       ( clock_48      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( osd_s & keys_s ),
    .conf_str      ( CONF_STR ),
    .status        ( status ),
    
    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);

reg port1_req, port2_req;
sdram sdram(
    .*,
    .init_n        ( pll_locked   ),
    .clk           ( clock_48     ),

    // port1 used for main CPU
    .port1_req     ( port1_req    ),
    .port1_ack     ( ),
    .port1_a       ( ioctl_addr[23:1] ),
    .port1_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port1_we      ( ioctl_downl ),
    .port1_d       ( {ioctl_dout, ioctl_dout} ),
    .port1_q       ( ),

    .cpu1_addr     ( ioctl_downl ? 15'h7fff : {1'b0, rom_addr[14:1]} ),
    .cpu1_q        ( rom_do ),

    // port2 for sound CPU
    .port2_req     ( port2_req ),
    .port2_ack     ( ),
    .port2_a       ( ioctl_addr[23:1] - 16'h8000 ),
    .port2_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port2_we      ( ioctl_downl ),
    .port2_d       ( {ioctl_dout, ioctl_dout} ),
    .port2_q       ( ),

    .snd_addr      ( ioctl_downl ? 15'h7fff : {3'b000, snd_addr[12:1]} ),
    .snd_q         ( snd_do )
);

always @(posedge clock_48) begin
    reg        ioctl_wr_last = 0;

    ioctl_wr_last <= ioctl_wr;
    if (ioctl_downl) begin
        if (~ioctl_wr_last && ioctl_wr) begin
            port1_req <= ~port1_req;
            port2_req <= ~port2_req;
        end
    end
end

reg reset = 1;
reg rom_loaded = 0;
always @(posedge clock_48) begin
    reg ioctl_downlD;
    ioctl_downlD <= ioctl_downl;

    if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
    reset <= status[0] | ~btn_n_i[4] | ioctl_downl | ~rom_loaded;
end

wire  [7:0] DSW0 = 0;//{2'h0,status[7:6],4'h0};
wire  [7:0] DSW1 = 0;
wire  [7:0] DSW2 = 0;//{4'h0,status[8],3'h0};

wire  [5:0] INP0 = { m_fireB, m_fireA, m_left, m_down, m_right, m_up};
wire  [5:0] INP1 = { m_fireB, m_fireA, m_left, m_down, m_right, m_up};
wire  [2:0] INP2 = { ~btn_n_i[3] | m_coin1, ~btn_n_i[2] | m_two_players , ~btn_n_i[1] | m_one_player };

wire  PCLK, PCLK_EN;
wire  [8:0] HPOS,VPOS;

fpga_druaga fpga_druaga(
    .MCLK(clock_48),
    .CLKCPUx2(clock_6),
    .RESET(reset),
    .SOUT(audio),
    .rom_addr(rom_addr),
    .rom_data(rom_addr[0] ? rom_do[15:8] : rom_do[7:0]),
    .snd_addr(snd_addr),
    .snd_data(snd_addr[0] ? snd_do[15:8] : snd_do[7:0]),
    .PH(HPOS),
    .PV(VPOS),
    .PCLK(PCLK),
    .PCLK_EN(PCLK_EN),
    .POUT({b,g,r}),
    .INP0(INP0),
    .INP1(INP1),
    .INP2(INP2),
    .DSW0(DSW0),
    .DSW1(DSW1),
    .DSW2(DSW2),

    .ROMAD(ioctl_addr[16:0]),
    .ROMDT(ioctl_dout),
    .ROMEN(ioctl_wr)
    );

hvgen hvgen(
    .MCLK(clock_48),
    .HPOS(HPOS),
    .VPOS(VPOS),
    .PCLK(PCLK),
    .PCLK_EN(PCLK_EN),
    .HBLK(hb),
    .VBLK(vb),
    .HSYN(hs),
    .VSYN(vs)
);

wire [7:0] vga_col_s;
wire vga_hs_s, vga_vs_s;

framebuffer #(288,224,8,1) framebuffer
(
        .clk_sys    ( clock_48 ),
        .clk_i      ( PCLK ),
        .RGB_i      ((blankn) ? {r,g,b} : 8'b00000000 ),
        .hblank_i   ( hb ),
        .vblank_i   ( vb ),
		  .dis_db_i   ( status[7] ),
        
        .rotate_i   ( status[2:1] ), 

        .clk_vga_i  ( (status[1]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);

wire osd_enable, direct_video;
assign scandoublerD = ~status[16] ^ direct_video;
wire [1:0] osd_rotate;

mist_video #(.COLOR_DEPTH(3),.SD_HCNT_WIDTH(10), .USE_FRAMEBUFFER(1)) mist_video(
    .clk_sys            ( (scandoublerD) ? clock_48 : (status[1]) ? clk_40 : clk_25m2),
    .SPI_SCK            ( SPI_SCK ),
    .SPI_SS3            ( SPI_SS2 ),
    .SPI_DI             ( SPI_DI ),
    .R                  ( (scandoublerD) ? blankn ? r : 0 : vga_col_s[7:5] ),
    .G                  ( (scandoublerD) ? blankn ? g : 0 : vga_col_s[4:2] ),
    .B                  ( (scandoublerD) ? blankn ? {b ,b[0]} : 0 : {vga_col_s[1:0],vga_col_s[0]} ),
    .HSync              ( (scandoublerD) ? ~hs : vga_hs_s ),
    .VSync              ( (scandoublerD) ? ~vs : vga_vs_s ),
    .VGA_R              ( VGA_R ),
    .VGA_G              ( VGA_G ),
    .VGA_B              ( VGA_B ),
    .VGA_VS             ( VGA_VS ),
    .VGA_HS             ( VGA_HS ),
    .rotate             ( osd_rotate ),
    .scandoubler_disable( scandoublerD ),
    .scanlines          ( status[4:3] ),
    .osd_enable         ( osd_enable ),
    .ce_divider         ( 1'b1 ),
    .blend              ( status[5] ),
    .patrons            ( status[6]      ),
    .PATRON_ADJ_X       ( -1000 ),
    .PATRON_ADJ_Y       ( 150 ),
    .PATRON_DOUBLE_HEIGHT(1 ),
    .PATRON_SCROLL      ( 1 )
    ); 

dac #(.C_bits(16))dac(
    .clk_i(clock_48),
    .res_n_i(1),
    .dac_i({audio,audio}),
    .dac_o(AUDIO_L)
    );

//-----------------------

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire btn_one_player =   ~btn_n_i[1] | m_one_player;
wire btn_two_players =  ~btn_n_i[2] | m_two_players;
wire btn_coin  =            ~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard #(.CLK_SPEED(48000))keyboard 
 (
  .clk       ( clock_48 ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

//translate scancode to joystick
kbd_joystick #( .OSD_CMD    ( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(48000)) k_joystick
(
    .clk            ( clock_48 ),
    .kbdint         ( kbd_intr ),
    .kbdscancode    ( kbd_scancode ),

    .joystick_0     ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1     ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),

    //-- joystick_0 and joystick_1 should be swapped
    .joyswap        ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer      ( 1 ),
    .direct_video   ( direct_video ),
    .osd_rotate     ( osd_rotate ),
    //-- tilt, coin4-1, start4-1
    .controls       ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right
    .player1        ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2        ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    //-- keys to the OSD
    .osd_o         ( keys_s ),
    .osd_enable    ( osd_enable ),

    //-- sega joystick
    .sega_strobe    ( joy_p7_o )
);

reg [7:0] osd_s = 8'b11111111;
PumpSignal PumpSignal (clock_48, ~pll_locked, ioctl_downl, osd_s);

endmodule
