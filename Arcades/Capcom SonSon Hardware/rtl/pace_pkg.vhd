--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;

package pace_pkg is

	--  
	-- PACE constants which *MUST* be defined
	--

  type PACETargetType is
  (
    PACE_TARGET_NANOBOARD_NB1,
    PACE_TARGET_DE0,
    PACE_TARGET_DE0_CV,		-- 5CEBA4
    PACE_TARGET_DE0_NANO,	-- EP4CE22
    PACE_TARGET_DE1,
    PACE_TARGET_DE2,
    PACE_TARGET_DE2_70,		-- EP2C70
    PACE_TARGET_DE2_115,	-- EP4CE115
    PACE_TARGET_P2,		-- A02 build
    PACE_TARGET_P2A,		-- A04/A build (SRAM byte selects)
    PACE_TARGET_P3M,
    PACE_TARGET_S3A_700,	-- Spartan 3A/N Starter Kit
    PACE_TARGET_RC10,
    PACE_TARGET_NX2_12,
    PACE_TARGET_NEXYS_3,	-- Digilent S6 board
    PACE_TARGET_CYC3DEV,
    PACE_TARGET_CYC5GXDEV,
    PACE_TARGET_COCO3PLUS,
    PACE_TARGET_S5A,
    PACE_TARGET_CARTEBLANCHE_250,
    PACE_TARGET_CARTEBLANCHE_500,
    PACE_TARGET_BEMICRO,
    PACE_TARGET_OPENEP3C16,
    PACE_TARGET_MIST,
    PACE_TARGET_CHAMELEON64,
    PACE_TARGET_RETRORAMBLINGS_CYC3,  -- Generic EP3C25 board with custom io boards
    PACE_TARGET_S5A_R2_EP4C,
    PACE_TARGET_S5A_R2_EP3SL,
    PACE_TARGET_S5A_R2B0_EP4C,
    PACE_TARGET_S5A_R2B0_EP3SL,
    PACE_TARGET_S5A_R2C0_EP4C,
    PACE_TARGET_S5A_R2C0_EP3SL,
    PACE_TARGET_S5L_A0_EP4C,
    PACE_TARGET_S5L_A0_EP3SL,
    PACE_TARGET_NAVICO_ROCKY,
    PACE_TARGET_NGPACE,
    PACE_TARGET_S6M_A0
  );

	type PACEFpgaVendor_t is
	(
		PACE_FPGA_VENDOR_ALTERA,
		PACE_FPGA_VENDOR_XILINX,
		PACE_FPGA_VENDOR_LATTICE
	);

	type PACEFpgaFamily_t is
	(
		PACE_FPGA_FAMILY_CYCLONE1,
		PACE_FPGA_FAMILY_CYCLONE2,
		PACE_FPGA_FAMILY_CYCLONE3,
		PACE_FPGA_FAMILY_CYCLONE4,
		PACE_FPGA_FAMILY_CYCLONE5,
		PACE_FPGA_FAMILY_CYCLONE6,
		PACE_FPGA_FAMILY_STRATIX_III,
		PACE_FPGA_FAMILY_SPARTAN3,
		PACE_FPGA_FAMILY_SPARTAN3A,
		PACE_FPGA_FAMILY_SPARTAN3E
	);

  type PACEJamma_t is
  (
    PACE_JAMMA_NONE,
    PACE_JAMMA_MAPLE,
    PACE_JAMMA_NGC,
    PACE_JAMMA_PS2
  );

  -- Types

	type ByteArrayType is array (natural range <>) of std_logic_vector(7 downto 0);

  type from_CLKRST_t is record
    arst      : std_logic;
    arst_n    : std_logic;
    rst       : std_logic_vector(0 to 3);
    clk_ref   : std_logic;                  --reference clock
    clk       : std_logic_vector(0 to 3);
  end record;
  


  
  type from_AUDIO_t is record
    clk       : std_logic;
  end record;
  
  type to_AUDIO_t is record
    clk       : std_logic;
    ldata     : std_logic_vector(15 downto 0);
    rdata     : std_logic_vector(15 downto 0);
  end record;
  
  function NULL_TO_AUDIO return to_AUDIO_t;

  type from_SPI_t is record
    din       : std_logic;
  end record;
  
  type to_SPI_t is record
    clk       : std_logic;
    mode      : std_logic;
    sel       : std_logic;
    ena       : std_logic;
    dout      : std_logic;
  end record;
  
  subtype SND_A_t is std_logic_vector(7 downto 0);
  subtype SND_D_t is std_logic_vector(7 downto 0);
  
  type to_SOUND_t is record
    a         : SND_A_t;
    d         : SND_D_t;
    rd        : std_logic;
    wr        : std_logic;
  end record;
  
  type from_SOUND_t is record
    d         : SND_D_t;
  end record;

  function NULL_TO_SOUND return to_SOUND_t;

	-- create a constant that automatically determines 
	-- whether this is simulation or synthesis
	constant IN_SIMULATION : BOOLEAN := false
	-- synthesis translate_off
	or true
	-- synthesis translate_on
	;
	constant IN_SYNTHESIS : boolean := not IN_SIMULATION;
	
end;
