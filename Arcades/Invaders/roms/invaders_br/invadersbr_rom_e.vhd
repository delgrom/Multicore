--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- generated with romgen by MikeJ
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity INVADERSBR_ROM_E is
  port (
    CLK         : in    std_logic;
    ADDR        : in    std_logic_vector(10 downto 0);
    DATA        : out   std_logic_vector(7 downto 0)
    );
end;

architecture RTL of INVADERSBR_ROM_E is


  type ROM_ARRAY is array(0 to 2047) of std_logic_vector(7 downto 0);
  constant ROM : ROM_ARRAY := (
    x"01",x"01",x"01",x"01",x"01",x"01",x"01",x"01", -- 0x0000
    x"01",x"01",x"01",x"01",x"01",x"01",x"01",x"01", -- 0x0008
    x"01",x"01",x"01",x"01",x"01",x"01",x"03",x"03", -- 0x0010
    x"03",x"03",x"03",x"03",x"03",x"03",x"03",x"03", -- 0x0018
    x"03",x"03",x"03",x"03",x"03",x"03",x"03",x"03", -- 0x0020
    x"03",x"03",x"03",x"03",x"01",x"01",x"01",x"01", -- 0x0028
    x"01",x"01",x"01",x"01",x"01",x"01",x"01",x"01", -- 0x0030
    x"01",x"01",x"01",x"03",x"03",x"03",x"01",x"01", -- 0x0038
    x"01",x"01",x"05",x"05",x"00",x"03",x"03",x"00", -- 0x0040
    x"03",x"03",x"00",x"05",x"05",x"05",x"05",x"05", -- 0x0048
    x"05",x"05",x"05",x"05",x"05",x"05",x"05",x"05", -- 0x0050
    x"01",x"03",x"01",x"01",x"01",x"01",x"01",x"01", -- 0x0058
    x"01",x"03",x"01",x"01",x"01",x"00",x"03",x"03", -- 0x0060
    x"03",x"03",x"03",x"00",x"01",x"01",x"07",x"00", -- 0x0068
    x"05",x"00",x"03",x"05",x"03",x"00",x"05",x"00", -- 0x0070
    x"07",x"07",x"07",x"03",x"00",x"05",x"05",x"05", -- 0x0078
    x"00",x"03",x"07",x"07",x"01",x"01",x"01",x"01", -- 0x0080
    x"01",x"01",x"01",x"01",x"01",x"01",x"01",x"01", -- 0x0088
    x"03",x"03",x"03",x"00",x"01",x"00",x"03",x"03", -- 0x0090
    x"03",x"01",x"05",x"05",x"05",x"00",x"03",x"03", -- 0x0098
    x"03",x"00",x"05",x"05",x"05",x"07",x"07",x"07", -- 0x00A0
    x"07",x"07",x"07",x"07",x"07",x"07",x"07",x"07", -- 0x00A8
    x"01",x"01",x"03",x"03",x"03",x"03",x"03",x"03", -- 0x00B0
    x"03",x"01",x"01",x"01",x"01",x"03",x"03",x"03", -- 0x00B8
    x"03",x"03",x"03",x"03",x"01",x"01",x"05",x"03", -- 0x00C0
    x"03",x"03",x"03",x"03",x"03",x"03",x"03",x"03", -- 0x00C8
    x"05",x"05",x"05",x"05",x"05",x"03",x"03",x"03", -- 0x00D0
    x"05",x"05",x"05",x"05",x"21",x"17",x"26",x"11", -- 0x00D8
    x"E4",x"41",x"01",x"18",x"08",x"CD",x"CD",x"41", -- 0x00E0
    x"CD",x"9D",x"0C",x"21",x"17",x"26",x"11",x"EC", -- 0x00E8
    x"41",x"01",x"10",x"06",x"CD",x"48",x"41",x"CD", -- 0x00F0
    x"9D",x"0C",x"21",x"0D",x"26",x"11",x"E4",x"41", -- 0x00F8
    x"01",x"18",x"04",x"CD",x"CD",x"41",x"CD",x"9D", -- 0x0100
    x"0C",x"21",x"0D",x"26",x"11",x"4C",x"42",x"01", -- 0x0108
    x"08",x"06",x"CD",x"48",x"41",x"CD",x"9D",x"0C", -- 0x0110
    x"21",x"06",x"2E",x"11",x"7C",x"42",x"01",x"02", -- 0x0118
    x"38",x"CD",x"33",x"41",x"CD",x"9D",x"0C",x"CD", -- 0x0120
    x"19",x"17",x"CD",x"D2",x"0D",x"CD",x"9D",x"0C", -- 0x0128
    x"C3",x"A3",x"0B",x"C5",x"E5",x"1A",x"77",x"23", -- 0x0130
    x"13",x"0D",x"C2",x"35",x"41",x"E1",x"01",x"20", -- 0x0138
    x"00",x"09",x"C1",x"05",x"C2",x"33",x"41",x"C9", -- 0x0140
    x"E5",x"C5",x"D5",x"0E",x"08",x"1A",x"0F",x"F5", -- 0x0148
    x"D2",x"A4",x"41",x"C5",x"0E",x"04",x"11",x"20", -- 0x0150
    x"00",x"19",x"0D",x"C2",x"59",x"41",x"C1",x"F1", -- 0x0158
    x"0D",x"C2",x"4E",x"41",x"D1",x"13",x"05",x"C2", -- 0x0160
    x"4A",x"41",x"21",x"B0",x"20",x"34",x"7E",x"E6", -- 0x0168
    x"02",x"CA",x"7C",x"41",x"36",x"00",x"C1",x"E1", -- 0x0170
    x"2B",x"C3",x"7E",x"41",x"C1",x"E1",x"0D",x"C2", -- 0x0178
    x"48",x"41",x"AF",x"32",x"B0",x"20",x"26",x"1F", -- 0x0180
    x"2E",x"E4",x"24",x"2D",x"7E",x"A7",x"C8",x"CD", -- 0x0188
    x"49",x"1F",x"E6",x"76",x"FE",x"36",x"C0",x"2E", -- 0x0190
    x"EC",x"2D",x"26",x"1F",x"24",x"36",x"02",x"2E", -- 0x0198
    x"CC",x"36",x"02",x"C9",x"C5",x"0E",x"03",x"3A", -- 0x01A0
    x"B0",x"20",x"FE",x"01",x"C2",x"C8",x"41",x"3E", -- 0x01A8
    x"E0",x"46",x"A0",x"77",x"11",x"20",x"00",x"19", -- 0x01B0
    x"0D",x"C2",x"B1",x"41",x"19",x"01",x"50",x"02", -- 0x01B8
    x"CD",x"C9",x"0E",x"D3",x"06",x"C3",x"5E",x"41", -- 0x01C0
    x"3E",x"0E",x"C3",x"B1",x"41",x"E5",x"C5",x"D5", -- 0x01C8
    x"D3",x"06",x"06",x"08",x"CD",x"83",x"11",x"D1", -- 0x01D0
    x"0D",x"C2",x"CF",x"41",x"C1",x"E1",x"2B",x"05", -- 0x01D8
    x"C2",x"CD",x"41",x"C9",x"EE",x"EE",x"EE",x"00", -- 0x01E0
    x"EE",x"EE",x"EE",x"00",x"1E",x"FC",x"C1",x"07", -- 0x01E8
    x"1E",x"FE",x"7F",x"FC",x"C3",x"07",x"3F",x"FE", -- 0x01F0
    x"FF",x"FC",x"E7",x"8F",x"7F",x"FE",x"FF",x"FC", -- 0x01F8
    x"E7",x"8F",x"7F",x"FE",x"E3",x"1C",x"F7",x"9F", -- 0x0200
    x"73",x"07",x"E7",x"1C",x"F7",x"9F",x"73",x"07", -- 0x0208
    x"0E",x"1C",x"77",x"9D",x"03",x"07",x"3E",x"FC", -- 0x0210
    x"77",x"9D",x"03",x"1F",x"FC",x"F8",x"F3",x"DF", -- 0x0218
    x"81",x"1F",x"F0",x"F9",x"C1",x"C7",x"81",x"1F", -- 0x0220
    x"C0",x"39",x"E0",x"CE",x"81",x"01",x"98",x"39", -- 0x0228
    x"E0",x"CE",x"9D",x"01",x"98",x"39",x"70",x"DC", -- 0x0230
    x"DD",x"01",x"F8",x"39",x"70",x"DC",x"DF",x"0F", -- 0x0238
    x"F0",x"39",x"E0",x"8E",x"CF",x"0F",x"E0",x"38", -- 0x0240
    x"E0",x"0E",x"C7",x"0F",x"36",x"9B",x"39",x"CF", -- 0x0248
    x"F7",x"38",x"36",x"9B",x"39",x"DB",x"B0",x"6D", -- 0x0250
    x"76",x"9B",x"6D",x"DB",x"B0",x"0D",x"F6",x"9B", -- 0x0258
    x"6D",x"DB",x"B0",x"0D",x"F6",x"F3",x"7C",x"DB", -- 0x0260
    x"F3",x"38",x"B6",x"F3",x"6C",x"DB",x"B0",x"61", -- 0x0268
    x"36",x"63",x"6C",x"DB",x"B0",x"6D",x"36",x"63", -- 0x0270
    x"6C",x"CF",x"B7",x"39",x"FF",x"FF",x"FF",x"FF", -- 0x0278
    x"FF",x"FF",x"C0",x"F1",x"C0",x"F1",x"C0",x"FF", -- 0x0280
    x"80",x"7F",x"00",x"3F",x"00",x"00",x"00",x"00", -- 0x0288
    x"FF",x"0F",x"FF",x"3F",x"FF",x"FF",x"C0",x"F1", -- 0x0290
    x"C0",x"F1",x"FF",x"FF",x"FF",x"3F",x"FF",x"0F", -- 0x0298
    x"00",x"00",x"00",x"00",x"FF",x"FF",x"FF",x"FF", -- 0x02A0
    x"FF",x"FF",x"C0",x"F1",x"E0",x"F1",x"FF",x"FF", -- 0x02A8
    x"BF",x"7F",x"1F",x"3F",x"00",x"00",x"00",x"00", -- 0x02B0
    x"00",x"F0",x"00",x"F0",x"00",x"F0",x"FF",x"FF", -- 0x02B8
    x"FF",x"FF",x"FF",x"FF",x"00",x"F0",x"00",x"F0", -- 0x02C0
    x"00",x"F0",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x02C8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x02D0
    x"07",x"F0",x"FF",x"FF",x"FF",x"FF",x"FF",x"FF", -- 0x02D8
    x"07",x"F0",x"07",x"F0",x"FF",x"FF",x"FF",x"FF", -- 0x02E0
    x"FF",x"FF",x"07",x"F0",x"CD",x"F9",x"07",x"FE", -- 0x02E8
    x"02",x"D8",x"FE",x"04",x"D2",x"FC",x"42",x"3A", -- 0x02F0
    x"B8",x"20",x"A7",x"C0",x"3A",x"AD",x"20",x"A7", -- 0x02F8
    x"C0",x"CD",x"BB",x"43",x"21",x"A1",x"20",x"7E", -- 0x0300
    x"A7",x"C2",x"5E",x"43",x"2B",x"7E",x"E6",x"80", -- 0x0308
    x"C8",x"3A",x"A0",x"20",x"E6",x"0F",x"47",x"3A", -- 0x0310
    x"0A",x"20",x"05",x"CA",x"23",x"43",x"C6",x"10", -- 0x0318
    x"C3",x"1A",x"43",x"C6",x"04",x"32",x"A5",x"20", -- 0x0320
    x"DE",x"08",x"E6",x"FE",x"47",x"21",x"8A",x"20", -- 0x0328
    x"7E",x"E6",x"FE",x"B8",x"C0",x"3A",x"09",x"20", -- 0x0330
    x"C6",x"40",x"32",x"A9",x"20",x"3A",x"06",x"20", -- 0x0338
    x"21",x"AA",x"20",x"BE",x"D2",x"59",x"43",x"06", -- 0x0340
    x"02",x"3A",x"0D",x"20",x"A7",x"C2",x"52",x"43", -- 0x0348
    x"06",x"FE",x"3A",x"A5",x"20",x"80",x"32",x"A5", -- 0x0350
    x"20",x"3E",x"01",x"32",x"A1",x"20",x"21",x"A4", -- 0x0358
    x"20",x"CD",x"BB",x"17",x"EB",x"3A",x"A9",x"20", -- 0x0360
    x"BD",x"DA",x"79",x"43",x"2A",x"AA",x"20",x"36", -- 0x0368
    x"05",x"CD",x"A3",x"43",x"3E",x"01",x"32",x"B7", -- 0x0370
    x"20",x"21",x"20",x"20",x"CD",x"91",x"02",x"E1", -- 0x0378
    x"C3",x"94",x"00",x"21",x"A1",x"20",x"7E",x"A7", -- 0x0380
    x"C8",x"21",x"A2",x"20",x"4E",x"CD",x"1F",x"02", -- 0x0388
    x"21",x"A4",x"20",x"CD",x"BB",x"17",x"EB",x"7D", -- 0x0390
    x"FE",x"2F",x"D2",x"AE",x"43",x"11",x"8C",x"45", -- 0x0398
    x"CD",x"96",x"13",x"06",x"0E",x"21",x"A0",x"20", -- 0x03A0
    x"11",x"A0",x"1B",x"C3",x"B2",x"17",x"3A",x"AD", -- 0x03A8
    x"20",x"A7",x"CA",x"B8",x"43",x"11",x"94",x"45", -- 0x03B0
    x"C3",x"96",x"13",x"3A",x"84",x"20",x"A7",x"CA", -- 0x03B8
    x"FF",x"43",x"3A",x"A1",x"20",x"A7",x"C0",x"4F", -- 0x03C0
    x"3A",x"8C",x"20",x"FE",x"02",x"79",x"3A",x"67", -- 0x03C8
    x"20",x"67",x"06",x"0B",x"C2",x"E4",x"43",x"2E", -- 0x03D0
    x"2C",x"7E",x"A7",x"CA",x"F1",x"43",x"23",x"05", -- 0x03D8
    x"C2",x"D9",x"43",x"C9",x"2E",x"36",x"7E",x"A7", -- 0x03E0
    x"CA",x"F1",x"43",x"2B",x"05",x"C2",x"E6",x"43", -- 0x03E8
    x"C9",x"22",x"AA",x"20",x"7D",x"C6",x"05",x"E6", -- 0x03F0
    x"0F",x"F6",x"80",x"32",x"A0",x"20",x"C9",x"3A", -- 0x03F8
    x"A0",x"20",x"E6",x"7F",x"32",x"A0",x"20",x"21", -- 0x0400
    x"B7",x"20",x"7E",x"A7",x"C8",x"23",x"77",x"C9", -- 0x0408
    x"3A",x"AE",x"20",x"A7",x"C8",x"CD",x"58",x"17", -- 0x0410
    x"CD",x"53",x"46",x"22",x"F2",x"20",x"3E",x"01", -- 0x0418
    x"32",x"F1",x"20",x"CD",x"58",x"0B",x"3E",x"02", -- 0x0420
    x"32",x"C0",x"20",x"21",x"04",x"29",x"11",x"08", -- 0x0428
    x"45",x"0E",x"11",x"CD",x"D9",x"0A",x"01",x"B4", -- 0x0430
    x"20",x"0A",x"A7",x"11",x"C6",x"09",x"C2",x"49", -- 0x0438
    x"44",x"0B",x"0A",x"A7",x"CA",x"51",x"44",x"13", -- 0x0440
    x"13",x"21",x"04",x"2F",x"0E",x"02",x"CD",x"D9", -- 0x0448
    x"0A",x"21",x"10",x"23",x"11",x"04",x"45",x"06", -- 0x0450
    x"04",x"CD",x"B2",x"17",x"2A",x"0B",x"20",x"3E", -- 0x0458
    x"07",x"84",x"67",x"22",x"12",x"23",x"CD",x"89", -- 0x0460
    x"44",x"CD",x"E3",x"44",x"7D",x"FE",x"C8",x"D2", -- 0x0468
    x"EB",x"44",x"CD",x"CF",x"44",x"E5",x"CD",x"A3", -- 0x0470
    x"44",x"E1",x"CD",x"B9",x"44",x"21",x"10",x"23", -- 0x0478
    x"34",x"23",x"34",x"CD",x"E0",x"44",x"C3",x"6C", -- 0x0480
    x"44",x"CD",x"CF",x"44",x"3A",x"C0",x"20",x"A7", -- 0x0488
    x"CA",x"9B",x"44",x"3E",x"02",x"32",x"C0",x"20", -- 0x0490
    x"C3",x"3A",x"16",x"3E",x"00",x"32",x"C0",x"20", -- 0x0498
    x"C3",x"BA",x"07",x"3A",x"10",x"23",x"4F",x"3A", -- 0x04A0
    x"11",x"23",x"47",x"7C",x"90",x"67",x"FE",x"38", -- 0x04A8
    x"D8",x"CD",x"CF",x"44",x"0D",x"C2",x"A7",x"44", -- 0x04B0
    x"C9",x"3A",x"10",x"23",x"4F",x"3A",x"11",x"23", -- 0x04B8
    x"47",x"7C",x"80",x"67",x"FE",x"E8",x"D0",x"CD", -- 0x04C0
    x"CF",x"44",x"0D",x"C2",x"BD",x"44",x"C9",x"E5", -- 0x04C8
    x"CD",x"C7",x"17",x"3E",x"0F",x"77",x"E1",x"C5", -- 0x04D0
    x"01",x"20",x"03",x"CD",x"C9",x"0E",x"C1",x"C9", -- 0x04D8
    x"2A",x"12",x"23",x"3E",x"08",x"85",x"6F",x"22", -- 0x04E0
    x"12",x"23",x"C9",x"21",x"BE",x"20",x"34",x"3A", -- 0x04E8
    x"B5",x"20",x"BE",x"DA",x"FC",x"44",x"CD",x"A3", -- 0x04F0
    x"0B",x"C3",x"2B",x"44",x"36",x"00",x"CD",x"BA", -- 0x04F8
    x"07",x"C3",x"A2",x"0C",x"01",x"02",x"00",x"00", -- 0x0500
    x"01",x"0E",x"0D",x"14",x"12",x"1B",x"1B",x"21", -- 0x0508
    x"1C",x"1C",x"1B",x"0F",x"0E",x"0D",x"13",x"0E", -- 0x0510
    x"12",x"26",x"20",x"2E",x"1E",x"7E",x"A7",x"C2", -- 0x0518
    x"2E",x"45",x"CD",x"49",x"1F",x"E6",x"76",x"D6", -- 0x0520
    x"52",x"C0",x"3C",x"32",x"1E",x"20",x"CD",x"49", -- 0x0528
    x"1F",x"E6",x"76",x"FE",x"34",x"C0",x"26",x"2C", -- 0x0530
    x"2E",x"1B",x"16",x"0D",x"1E",x"78",x"0E",x"0A", -- 0x0538
    x"0C",x"1A",x"C6",x"3F",x"D5",x"CD",x"E5",x"0A", -- 0x0540
    x"D1",x"1B",x"0D",x"C2",x"41",x"45",x"C9",x"21", -- 0x0548
    x"D0",x"DB",x"01",x"DB",x"C8",x"11",x"4C",x"17", -- 0x0550
    x"1A",x"FE",x"D6",x"C0",x"13",x"1A",x"BC",x"C0", -- 0x0558
    x"13",x"1A",x"BD",x"C0",x"13",x"1A",x"B8",x"C0", -- 0x0560
    x"13",x"1A",x"B9",x"C0",x"2E",x"E2",x"26",x"20", -- 0x0568
    x"36",x"00",x"C9",x"26",x"28",x"2E",x"03",x"1E", -- 0x0570
    x"51",x"16",x"17",x"0E",x"12",x"0C",x"1A",x"C6", -- 0x0578
    x"38",x"D5",x"CD",x"E5",x"0A",x"D1",x"1B",x"0D", -- 0x0580
    x"C2",x"7E",x"45",x"C9",x"18",x"3C",x"7E",x"DB", -- 0x0588
    x"FF",x"24",x"5A",x"A5",x"98",x"5C",x"B6",x"5F", -- 0x0590
    x"5F",x"B6",x"5C",x"98",x"DB",x"00",x"E6",x"0A", -- 0x0598
    x"FE",x"0A",x"CA",x"00",x"00",x"A7",x"00",x"C0", -- 0x05A0
    x"21",x"E3",x"20",x"36",x"00",x"C9",x"1B",x"13", -- 0x05A8
    x"04",x"12",x"13",x"00",x"0D",x"03",x"0E",x"1B", -- 0x05B0
    x"2D",x"21",x"15",x"2D",x"11",x"AE",x"45",x"0E", -- 0x05B8
    x"0B",x"CD",x"85",x"0C",x"DB",x"01",x"E6",x"76", -- 0x05C0
    x"FE",x"10",x"C8",x"FE",x"04",x"CC",x"E4",x"45", -- 0x05C8
    x"FE",x"20",x"CC",x"F9",x"45",x"FE",x"02",x"CC", -- 0x05D0
    x"FF",x"45",x"FE",x"40",x"CC",x"31",x"46",x"D3", -- 0x05D8
    x"06",x"C3",x"C4",x"45",x"DB",x"01",x"E6",x"04", -- 0x05E0
    x"C2",x"E4",x"45",x"21",x"00",x"01",x"22",x"F2", -- 0x05E8
    x"20",x"3E",x"01",x"32",x"F1",x"20",x"C3",x"58", -- 0x05F0
    x"0B",x"21",x"E4",x"20",x"36",x"01",x"C9",x"DB", -- 0x05F8
    x"01",x"E6",x"02",x"C2",x"FF",x"45",x"CD",x"F9", -- 0x0600
    x"07",x"FE",x"12",x"DA",x"10",x"46",x"36",x"09", -- 0x0608
    x"34",x"2E",x"FA",x"34",x"7E",x"FE",x"0A",x"C2", -- 0x0610
    x"1C",x"46",x"36",x"00",x"CD",x"59",x"02",x"CD", -- 0x0618
    x"97",x"09",x"CD",x"DE",x"01",x"CD",x"C2",x"16", -- 0x0620
    x"CD",x"F9",x"13",x"2E",x"FE",x"7E",x"C3",x"21", -- 0x0628
    x"0C",x"21",x"E4",x"20",x"36",x"00",x"C9",x"CD", -- 0x0630
    x"17",x"0B",x"34",x"7E",x"F5",x"21",x"01",x"25", -- 0x0638
    x"24",x"24",x"3D",x"C2",x"40",x"46",x"06",x"10", -- 0x0640
    x"11",x"81",x"1C",x"CD",x"83",x"11",x"F1",x"3C", -- 0x0648
    x"C3",x"0B",x"18",x"21",x"B3",x"20",x"7E",x"A7", -- 0x0650
    x"C2",x"65",x"46",x"23",x"7E",x"A7",x"C2",x"6E", -- 0x0658
    x"46",x"21",x"50",x"00",x"C9",x"21",x"00",x"01", -- 0x0660
    x"3E",x"04",x"32",x"B5",x"20",x"C9",x"21",x"80", -- 0x0668
    x"00",x"3E",x"03",x"32",x"B5",x"20",x"C9",x"32", -- 0x0670
    x"B3",x"20",x"32",x"AE",x"20",x"C9",x"32",x"B4", -- 0x0678
    x"20",x"C9",x"CD",x"F9",x"13",x"2E",x"F8",x"7E", -- 0x0680
    x"FE",x"38",x"D0",x"CD",x"17",x"0B",x"34",x"7E", -- 0x0688
    x"FE",x"07",x"D0",x"CD",x"3C",x"46",x"CD",x"3C", -- 0x0690
    x"0B",x"01",x"00",x"00",x"CD",x"C9",x"0E",x"06", -- 0x0698
    x"EF",x"C3",x"5C",x"17",x"42",x"30",x"48",x"2C", -- 0x06A0
    x"30",x"42",x"30",x"48",x"2C",x"30",x"42",x"30", -- 0x06A8
    x"48",x"0D",x"0A",x"09",x"44",x"42",x"09",x"32", -- 0x06B0
    x"43",x"48",x"2C",x"35",x"38",x"48",x"2C",x"35", -- 0x06B8
    x"38",x"48",x"2C",x"38",x"34",x"48",x"0D",x"0A", -- 0x06C0
    x"09",x"44",x"42",x"09",x"38",x"34",x"48",x"0D", -- 0x06C8
    x"0A",x"3B",x"0D",x"0A",x"3B",x"0D",x"0A",x"3B", -- 0x06D0
    x"09",x"2A",x"2A",x"2A",x"2A",x"2A",x"20",x"43", -- 0x06D8
    x"48",x"41",x"52",x"41",x"43",x"54",x"45",x"52", -- 0x06E0
    x"20",x"44",x"41",x"54",x"41",x"20",x"2A",x"2A", -- 0x06E8
    x"2A",x"2A",x"2A",x"0D",x"0A",x"3B",x"0D",x"0A", -- 0x06F0
    x"43",x"48",x"52",x"4F",x"4D",x"3A",x"09",x"44", -- 0x06F8
    x"42",x"09",x"31",x"46",x"48",x"2C",x"32",x"34", -- 0x0700
    x"48",x"2C",x"34",x"34",x"48",x"09",x"20",x"20", -- 0x0708
    x"20",x"3B",x"20",x"20",x"41",x"20",x"20",x"20", -- 0x0710
    x"3C",x"30",x"30",x"3E",x"0D",x"0A",x"09",x"44", -- 0x0718
    x"42",x"09",x"32",x"34",x"48",x"2C",x"31",x"46", -- 0x0720
    x"48",x"0D",x"0A",x"09",x"44",x"42",x"09",x"37", -- 0x0728
    x"46",x"48",x"2C",x"34",x"39",x"48",x"2C",x"34", -- 0x0730
    x"39",x"48",x"09",x"20",x"20",x"20",x"3B",x"20", -- 0x0738
    x"20",x"42",x"20",x"20",x"20",x"3C",x"30",x"31", -- 0x0740
    x"3E",x"0D",x"0A",x"09",x"44",x"42",x"09",x"34", -- 0x0748
    x"39",x"48",x"2C",x"33",x"36",x"48",x"0D",x"0A", -- 0x0750
    x"09",x"44",x"42",x"09",x"33",x"45",x"48",x"2C", -- 0x0758
    x"34",x"31",x"48",x"2C",x"34",x"31",x"48",x"09", -- 0x0760
    x"20",x"20",x"20",x"3B",x"20",x"20",x"43",x"20", -- 0x0768
    x"20",x"20",x"3C",x"30",x"32",x"3E",x"0D",x"0A", -- 0x0770
    x"09",x"44",x"42",x"09",x"34",x"31",x"48",x"2C", -- 0x0778
    x"32",x"32",x"48",x"0D",x"0A",x"09",x"44",x"42", -- 0x0780
    x"09",x"37",x"46",x"48",x"2C",x"34",x"31",x"48", -- 0x0788
    x"2C",x"34",x"31",x"48",x"09",x"20",x"20",x"20", -- 0x0790
    x"3B",x"20",x"20",x"44",x"20",x"20",x"20",x"3C", -- 0x0798
    x"30",x"33",x"3E",x"0D",x"0A",x"09",x"44",x"42", -- 0x07A0
    x"09",x"34",x"31",x"48",x"2C",x"33",x"45",x"48", -- 0x07A8
    x"0D",x"0A",x"09",x"44",x"42",x"09",x"37",x"46", -- 0x07B0
    x"48",x"2C",x"34",x"39",x"48",x"2C",x"34",x"39", -- 0x07B8
    x"48",x"09",x"20",x"20",x"20",x"3B",x"20",x"20", -- 0x07C0
    x"45",x"20",x"20",x"20",x"3C",x"30",x"34",x"3E", -- 0x07C8
    x"0D",x"0A",x"09",x"44",x"42",x"09",x"34",x"39", -- 0x07D0
    x"48",x"2C",x"34",x"31",x"48",x"0D",x"0A",x"09", -- 0x07D8
    x"44",x"42",x"09",x"37",x"46",x"48",x"2C",x"34", -- 0x07E0
    x"38",x"48",x"2C",x"34",x"38",x"48",x"09",x"20", -- 0x07E8
    x"20",x"20",x"3B",x"20",x"20",x"46",x"20",x"20", -- 0x07F0
    x"20",x"3C",x"30",x"35",x"3E",x"0D",x"0A",x"09"  -- 0x07F8
  );

begin

  p_rom : process
  begin
    wait until rising_edge(CLK);
     DATA <= ROM(to_integer(unsigned(ADDR)));
  end process;
end RTL;
