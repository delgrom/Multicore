/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//********************************************************
	FPGA Implimentation of "Green Beret"  (Sound Part)
*********************************************************/
// Copyright (c) 2013 MiSTer-X

module SOUND
(
	input				MCLK,
	input				reset,

	output  [7:0]	SNDOUT,

	input				CPUMX,
	input	 [15:0]	CPUAD,
	input				CPUWR,
	input	  [7:0]	CPUWD
);

wire CS_SNDLC = ( CPUAD[15:8] == 8'hF2 ) & CPUMX & CPUWR;
wire CS_SNDWR = ( CPUAD[15:8] == 8'hF4 ) & CPUMX;

reg [7:0] SNDLATCH;
always @( posedge MCLK or posedge reset ) begin
	if (reset) SNDLATCH <= 0;
	else begin
		if ( CS_SNDLC ) SNDLATCH <= CPUWD;
	end
end

wire sndclk, sndclk_en;
sndclkgen scgen( MCLK, sndclk, sndclk_en );

SN76496 sgn( MCLK, sndclk_en, reset, CS_SNDWR, CPUWR, SNDLATCH, 4'b1111, SNDOUT );

endmodule


/*
   Clock Generator
     in: 50000000Hz -> out: 1600000Hz
*/
module sndclkgen( input in, output reg out, output reg out_en );
reg [6:0] count;
always @( posedge in ) begin
				out_en <= 0;
        if (count > 7'd117) begin
                count <= count - 7'd117;
                out <= ~out;
								if (~out) out_en <= 1;
        end
        else count <= count + 7'd8;
end
endmodule
