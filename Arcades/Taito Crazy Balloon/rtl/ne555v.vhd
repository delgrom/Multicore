--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
--
-- NE555V implementation as frequency generator
--
-- basically a real easy way to get one clock from another!
--
-- Mike Coates 
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 

entity NE555V is
	generic (
		freq_in  : integer := 48000;
      freq_out : real := 4000.0;
      duty     : integer := 50
   );
	port (
		reset   : in  std_logic;  -- reset controller
      clk_in  : in  std_logic;  -- input frequency
      clk_out : out std_logic   -- output frequency
	);
end;

 
architecture behavior of NE555V is

	 constant maxcount  : integer := integer(real(freq_in)/freq_out) - 1;
	 constant cyclecount : integer := integer(real(maxcount) * ((100.0 - real(duty)) / 100.0));
	 signal r_reg, r_next: natural;

begin

	 process(clk_in,reset)
    begin

		if reset='0' then 
			r_reg <= 0;
      elsif falling_edge(clk_in) then 
			r_reg <= r_next;
      end if;
    end process;


    -- next state logic
    r_next <= 0 when r_reg=maxcount else r_reg+1;

    -- clk_out setting
    clk_out <= '0' when r_reg < cyclecount else '1';

end behavior;

