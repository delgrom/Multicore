quartus_sh -t rtl/build_id.tcl RAMPAGE 
quartus_sh --flow compile mcr3mono.qpf
copy output_files\mcr3mono.rbf output_files\Rampage.mc2

quartus_sh -t rtl/build_id.tcl MAXRPM 
quartus_sh --flow compile mcr3mono.qpf
copy output_files\mcr3mono.rbf output_files\MaxRPM.mc2

quartus_sh -t rtl/build_id.tcl SARGE 
quartus_sh --flow compile mcr3mono.qpf
copy output_files\mcr3mono.rbf output_files\Sarge.mc2

quartus_sh -t rtl/build_id.tcl POWERDRV 
quartus_sh --flow compile mcr3mono.qpf
copy output_files\mcr3mono.rbf output_files\PowerDrive.mc2

pause