/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//  Pacman
//
//  Version With Framebuffer and HDMI
//  Copyright (C) 2020 Victor Trucco
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

//============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none

module Pacman
(
    // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT        = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                    = 1'b1 // '0' is LED on

);


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------

assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;


//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clk_sys ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_sys, ~pll_locked, ioctl_downl, pump_s);

//-- END defaults -------------------------------------------------------


localparam CONF_STR = {
//  "P,JumpShot.dat;", //mod 16
// "P,TheGlob.dat;", //mod 15
//  "P,DreamShopper.dat;", //mod 14
//  "P,PacManiacMiner.dat;", //mod 13
//    "P,VanVanCar.dat;", //mod 12
//  "P,Ponpoko.dat;", //mod 11
//  "P,Alibaba.dat;", //mod 10
//  "P,Eeekk.dat;", //mod 9
//  "P,Woodpecker.dat;", //mod 8
//  "P,MrTnt.dat;", //mod 7
//  "P,Gorkans.dat;", //mod 6
//  "P,MsPacman.dat;", // mod 5
//  "P,Birdiy.dat;", // mod 4
//  "P,PacmanClub.dat;", // mod 2
//  "P,PacManPlus.dat;", // mod 1
//  "P,PacMan.dat;", // mod 0
    "P,CORE_NAME.dat;", 
    "S,DAT,Alternative ROM...;",
    "O34,Scanlines,Off,25%,50%,75%;", 
    "O5,Scandoubler,On,Off;",
    //"O5,Blend,Off,On;", //16
    "O78,Rotate Screen,Off,90,180,270;", 
    
    /*
    "OF,Cabinet,Cocktail,Upright;",
    "OGH,Coinage,Free Play,1c/1cr,1c/2cr,2c/1cr;",
    "OIJ,Lives,1,2,3,5;",
    "OKL,Bonus Life After,10000,15000,20000,None;",
    "OM,Difficulty,Hard,Normal;",
    "ON,Ghosts Names,Off,On;",
    */
    
    "T6,Reset;", // 9
    "V,v1.30." // 8
};

reg [6:0] mod;
reg mod_plus = 0;
reg mod_jmpst= 0;
reg mod_club = 0;
reg mod_orig = 0;
reg mod_bird = 0;
reg mod_ms   = 0;
reg mod_gork = 0;
reg mod_mrtnt= 0;
reg mod_woodp= 0;
reg mod_eeek = 0;
reg mod_alib = 0;
reg mod_ponp = 0;
reg mod_van  = 0;
reg mod_pmm  = 0;
reg mod_dshop= 0;
reg mod_glob = 0;

wire mod_gm = mod_gork | mod_mrtnt;

always @(posedge clk_sys) 
begin   
    mod_orig <= (mod == 0);
    mod_plus <= (mod == 1);
    mod_club <= (mod == 2);
    mod_bird <= (mod == 4);
    mod_ms   <= (mod == 5);
    mod_gork <= (mod == 6);
    mod_mrtnt<= (mod == 7);
    mod_woodp<= (mod == 8);
    mod_eeek <= (mod == 9);
    mod_alib <= (mod == 10);
    mod_ponp <= (mod == 11);
    mod_van  <= (mod == 12);
    mod_pmm  <= (mod == 13);
    mod_dshop<= (mod == 14);
    mod_glob <= (mod == 15);
    mod_jmpst<= (mod == 16);
end

//assign LED = 1;
assign AUDIO_R = AUDIO_L;

assign stm_rst_o = 1'bz;
assign SPI_nWAIT = 1'b1;
assign SDRAM_nCS = 1'b1;
assign SDRAM_nWE = 1'b1;


wire clk_sys, clk_snd, clk_25, clk_pixel;

wire pll_locked;


PLL24 PLL24
(
    .inclk0         ( clock_50_i ),
    .c0             ( clk_sys ),
    .locked         ( pll_locked )
);

wire clk_25m2,clk_40;
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

reg ce_12m;
always @(posedge clk_sys) begin
    ce_12m <= !ce_12m;
end

reg ce_6m;
always @(posedge clk_sys) begin
    reg [1:0] div;
    div <= div + 1'd1;
    ce_6m <= !div;
end

reg ce_4m;
always @(posedge clk_sys) begin
    reg [2:0] div;
    
    div <= div + 1'd1;
    if(div == 5) div <= 0;
    ce_4m <= !div;
end

reg ce_1m79;
always @(posedge clk_sys) begin
    reg [3:0] div;
    
    div <= div + 1'd1;
    if(div == 12) div <= 0;
    ce_1m79 <= !div;
end

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire [10:0] ps2_key;
wire  [9:0] audio;
wire            hs, vs;
wire            hb, vb;
wire        blankn = ~(hb | vb);
wire  [2:0] r,g;
wire  [1:0] b;
wire  [3:0] idx_col;

wire [7:0] in0xor = mod_ponp ? 8'hE0 : 8'hFF;
wire [7:0] in1xor = mod_ponp ? 8'h00 : 8'hFF;
reg           m_cheat = 1'b0;

pacmant pacmant
(
    .O_VIDEO_COL    ( idx_col ),
    .O_HSYNC        ( hs ),
    .O_VSYNC        ( vs ),
    .O_HBLANK       ( hb ),
    .O_VBLANK       ( vb ),
    .O_AUDIO        ( audio ),
    .PCLK           ( clk_pixel ),
    .in0( (in0xor ^ 
                                {
                                    mod_eeek & m_fire2A,
                                    mod_alib & m_fireA,
                                    btn_coin, 
                                    (( mod_orig | mod_plus | mod_ms | mod_bird | mod_alib | mod_woodp) & m_cheat ) | (( mod_ponp | mod_van | mod_dshop ) & m_fireA ),
                                    m_down,
                                    m_right,
                                    m_left,
                                    m_up
                                })),

    .in1({status[15], 7'b1111111} & (in1xor ^ 
                                {
                                    mod_gm & m_fire2A,
                                    btn_two_players         | ( mod_eeek  & m_fireA ) | ( mod_jmpst & m_fire2A ),
                                    btn_one_player          | ( mod_jmpst & m_fireA ),
                                    ( mod_gm & m_fireA ) | (( mod_alib | mod_ponp | mod_van | mod_dshop ) & m_fire2A ),
                                    m_down2,
                                     mod_pmm ? m_fireA : m_right2,
                                    ~mod_pmm & m_left2,
                                     m_up2
                                })),
                            

    .dipsw1         ( status[23:16]),
    .dipsw2         ( (mod_ponp | mod_van | mod_dshop) ? status[31:24] : 8'hFF ),
    
    //.in0(8'hff),
//  .in1(8'hff),
//  .dipsw1(8'hff),
    //.dipsw2(8'hff),
    
    .RESET              ( status[6] | ~btn_n_i[4] | ioctl_downl ),
    .CLK                ( clk_sys ),
    .ENA_12             ( ce_12m ),
    .ENA_6              ( ce_6m ),
    .ENA_4              ( ce_4m ),
    .ENA_1M79           ( ce_1m79 ),
    
    // ---ROMS----
   
    .cpu_addr_o         ( cpu_addr_s ),
    .char_addr_o        ( char_addr_s ),
    .char_rom_5ef_dout  ( char_rom_5ef_dout ),

    .rom_addr           ( rom_addr_s ), 
    .rom_lo             ( rom_lo ),
    .rom_hi             ( rom_hi ),
    
    .video_x_o          ( video_x_s ),
    .video_y_o          ( video_y_s ),
    
    .mod_plus           ( mod_plus  ),
    .mod_jmpst          ( mod_jmpst ),
    .mod_bird           ( mod_bird  ),
    .mod_ms             ( mod_ms    ),
    .mod_mrtnt          ( mod_mrtnt ),
    .mod_woodp          ( mod_woodp ),
    .mod_eeek           ( mod_eeek ),
    .mod_alib           ( mod_alib ),
    .mod_ponp           ( mod_ponp | mod_van | mod_dshop ),
    .mod_van            ( mod_van | mod_dshop ),
    .mod_dshop          ( mod_dshop ),
    .mod_glob           ( mod_glob ),
    .mod_club           ( mod_club ),
    
    .dn_addr            ( ioctl_addr[15:0] ),
    .dn_data            ( ioctl_dout ),
    .dn_wr              ( ioctl_wr )
);

data_io #(.STRLEN(($size(CONF_STR)>>3))) 
data_io
(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( pump_s & keys_s ),
    .conf_str      ( CONF_STR ),
    .status        ( status ),
    .core_mod      ( mod ),
    
    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
    
);
    
dac #(.C_bits(11))
dac
(
    .clk_i   ( clk_sys ),
    .res_n_i ( 1 ),
    .dac_i   ( {1'b0, audio}), //audio} ),
    .dac_o   ( AUDIO_L )
);

//-----------------------

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_i[1] | m_one_player;
wire btn_two_players = ~btn_n_i[2] | m_two_players;
wire btn_coin        = ~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard #( .CLK_SPEED(24000)) keyboard 
 (
  .clk       ( clk_sys ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0] joy1_s;
wire [15:0] joy2_s;
wire [8:0]  controls_s;
wire        osd_enable, direct_video;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD    ( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(24000)) k_joystick
(
    .clk          ( clk_sys ),
    .kbdint       ( kbd_intr ),
    .kbdscancode  ( kbd_scancode ), 
    .joystick_0   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap      ( 0 ),
    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer    ( ~(mod_club | mod_pmm | mod_jmpst) ),
    .direct_video ( direct_video ),
    .osd_rotate       ( osd_rotate ),
    //-- fire12-1, up, down, left, right
    .player1      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),
    //-- tilt, coin4-1, start4-1
    .controls     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),
    //-- keys to the OSD
    .osd_o        ( keys_s ),
    .osd_enable   ( osd_enable ),
    //-- sega joystick
    .sega_strobe  ( joy_p7_o )
);

    //--------- RGB OUTPUT ----------------------------------------------------
    

    wire [7:0] rgb_out;
    
//  PROM7_DST   col_rom_rgb
//  (
//      .CLK   ( clk_sys ),
//      .ADDR  ( idx_col ),
//      .DATA  ( rgb_out )
//  );
    
    dpram #(4,8) col_rom_rgb
    (
        .clk_a_i    ( clk_sys ),
        .en_a_i     ( 1'b1 ),
        .we_i       ( ioctl_wr & rom7_cs & prom_cs ),
        .addr_a_i   ( ioctl_addr[3:0] ), 
        .data_a_i   ( ioctl_dout ),
    
        .clk_b_i    ( clk_pixel ),
        .addr_b_i   ( idx_col ),
        .data_b_o   ( rgb_out )
  );
  
    
    wire scandoublerD = ~status [ 5 ] ^ direct_video;
    
    //--------- VGA/HDMI OUTPUT ----------------------------------------------------
    wire [8:0] video_x_s;
    wire [7:0] video_y_s;

    wire [3:0] vga_col_s;
    
    //-- contador I_HCNT de 128 a 511 = 48 + 288 pixels horizontais
    //-- hsync de 175 a 207
    //-- hblank de 143 a 239
    //-- imagem de 239 a 511 (272 pixels) e de 128 a 143 (16 pixels)
    
    //-- contador I_VCNT de 248 a 511 = 39 + 224 pixels verticais   
/*    
    vga vga
    (
        .I_CLK      ( clk_sys ),
        .I_CLK_VGA  ( clk_pixel ),
        .I_COLOR    ( idx_col ),
        .I_HCNT     ( video_x_s ),
        .I_VCNT     ( video_y_s ),
        
        .I_ROTATE   ( status[8:7] ),
        
        .O_HSYNC    ( vga_hs_s ),
        .O_VSYNC    ( vga_vs_s ),
        .O_COLOR    ( vga_col_s ),
        .O_BLANK    ( vga_blank_s )
    );
 */   

    wire [7:0] vga_out;
        
//  PROM7_DST   col_rom_vga
//  (
//      .CLK   ( clk_pixel ),
//      .ADDR  ( vga_col_s ),
//      .DATA  ( vga_out )
//  );
    
    wire prom_cs;
    wire rom7_cs;

    assign prom_cs = (ioctl_addr[15:14] == 2'b11);   //0xc000-ffff 
    assign rom7_cs = (ioctl_addr[9:4] == 6'b110000); //0xc300 

    dpram #(4,8) col_rom_7f
    (
        .clk_a_i    ( clk_pixel ),
        .en_a_i     ( 1'b1 ),
        .we_i       ( ioctl_wr & rom7_cs & prom_cs ),
        .addr_a_i   ( ioctl_addr[3:0] ), 
        .data_a_i   ( ioctl_dout ),

        .clk_b_i    ( clk_pixel ),
        .addr_b_i   ( vga_col_s ),
        .data_b_o   ( vga_out )
    );
  
    assign {b,g,r} = vga_out;
    wire vga_hs_s;
    wire vga_vs_s;
    wire [7:0] vga_fb_out_s;

framebuffer #(288,224,8) framebuffer
(
        .clk_sys    ( clk_sys ),
        .clk_i      ( clk_pixel ),
        .RGB_i      ( (blankn) ? {rgb_out[2:0],rgb_out[5:3],rgb_out[7:6]} : 8'b00000000 ),
        .hblank_i   ( hb ),
        .vblank_i   ( vb ),
        
        .rotate_i   ( status[8:7] ), 

        .clk_vga_i  ( (status[7]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_fb_out_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);

wire [1:0] osd_rotate;

mist_video #(.COLOR_DEPTH(3),.SD_HCNT_WIDTH(10), .USE_FRAMEBUFFER(1)) mist_video(
    .clk_sys( (scandoublerD) ? clk_sys : (status[7]) ? clk_40 : clk_25m2),
    .SPI_SCK(SPI_SCK),
    .SPI_SS3(SPI_SS2),
    .SPI_DI(SPI_DI),
    .R((scandoublerD) ? blankn ? rgb_out[2:0] : 0 : vga_fb_out_s[7:5]),
    .G((scandoublerD) ? blankn ? rgb_out[5:3] : 0 : vga_fb_out_s[4:2]),
    .B((scandoublerD) ? blankn ? {rgb_out[7:6] ,rgb_out[6]} : 0 : {vga_fb_out_s[1:0],vga_fb_out_s[0]}),
    .HSync((scandoublerD) ? ~hs : vga_hs_s),
    .VSync((scandoublerD) ? ~vs : vga_vs_s),
    .VGA_R          ( VGA_R          ),
    .VGA_G          ( VGA_G          ),
    .VGA_B          ( VGA_B          ),
    .VGA_VS(VGA_VS),
    .VGA_HS(VGA_HS),
    .rotate         ( osd_rotate ),
    .scandoubler_disable(scandoublerD),
    .scanlines(status[4:3]),
    .osd_enable     ( osd_enable ),
    .ce_divider(1'b1),
     .blend          ( 1'b0 )
    ); 

    //-------------------
    // ROMS
    
    wire ioctl_downl;
    wire [7:0]ioctl_index;
    wire ioctl_wr;
    wire [24:0]ioctl_addr;
    wire [7:0]ioctl_dout;
    
    assign sram_oe_n_o  = 1'b0; 
    //assign program_rom_dinl = sram_data_io;   
    
//  assign sram_addr_o  = (ioctl_wr) ? ioctl_addr[18:0] : {5'b00000,cpu_addr_s[13:0] }; 
//  assign sram_data_io = (ioctl_wr) ? ioctl_dout : 8'hZZ;
//  assign program_rom_dinl = sram_data_io;
//  assign sram_we_n_o  = ~ioctl_wr;

     
    reg [7:0]rom_lo;
    reg [7:0]rom_hi;
    reg [15:0]rom_addr_s;
     
     
    always @(*)
    begin
        if (char_oe) char_rom_5ef_dout <= sram_data_io; 
    
        if (rom_addr_s[15]) rom_hi <= sram_data_io; else rom_lo <= sram_data_io; 
    end 
    
    always @(posedge clk_sys)
    begin
        sram_addr_o[20:19] <= 2'b00;                                    //0x8000 (graphics)         0x4000-7fff (HI rom)   0x0000-3fff (LO rom)
        sram_addr_o[18:0]  <= (ioctl_wr) ? ioctl_addr[18:0] : (ce_6m) ? {6'b000100,char_addr_s } : {4'b0000,rom_addr_s[15],rom_addr_s[13:0] };    
        //sram_addr_o  <= (ioctl_wr) ? ioctl_addr[18:0] : {5'b00000,cpu_addr_s[13:0] }; 
        sram_data_io <= (ioctl_wr) ? ioctl_dout : 8'hZZ;
        sram_we_n_o  <= ~ioctl_wr;
        
        char_oe <= ce_6m; //delay the clock signal one cycle to make the OE for video data
        
    end

    
    wire [15:0] cpu_addr_s;
    wire [12:0] char_addr_s;
    wire [7:0]  char_rom_5ef_dout;
    wire [7:0]  gfx_dout;
    reg char_oe;

endmodule 
